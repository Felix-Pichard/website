INSERT INTO article_topics (topic_id,title,slug,`order`,sidebar) VALUES 
(1,'Miscellaneous ','misc',99,0)
,(2,'Hacking','hacking',NULL,1)
,(3,'Network security','network-security',NULL,0)
,(5,'Privacy','privacy',NULL,1)
,(6,'Phreaking','phreaking',NULL,0)
,(7,'Lock picking','lock-picking',NULL,0)
,(8,'Talks / videos','talks-videos',NULL,0)
,(12,'Encryption and steganography','encryption-steganography',NULL,0)
,(13,'Coding','coding',NULL,1)
;

INSERT INTO `medals` (medal_id,label,colour,description,`type`,count,`order`) VALUES 
(4,'Devotee','bronze','Login everyday for a week','logins','7',5)
,(5,'Devotee','silver','Login everyday for a fortnight','logins','14',9)
,(6,'Devotee','gold','Login everyday for a month','logins','30',NULL)
,(7,'Veteran','bronze','Login after being registered for a month','member','30',10)
,(11,'Cheese','bronze','Change your profile image',NULL,NULL,NULL)
,(12,'Veteran','silver','Login after being registered for a year','member','365',NULL)
,(14,'Writer','silver','Write an article and get it published','articles','1',8)
,(15,'Helper','green','Make a considerable effort to help the site',NULL,NULL,NULL)
,(16,'Forum','bronze','Post 50 messages in the forum','posts','50',7)
,(17,'Forum','silver','Post 250 messages in the forum','posts','250',NULL)
,(18,'Forum','gold','Post 1000 messages in the forum','posts','1000',NULL)
,(19,'Donator','green','Make a donation greater than &pound;5','donate','5',NULL)
,(20,'To-do','bronze','Complete all items on the to-do list','todo','5',1)
# ,(21,'+ve','bronze','Get awarded your first positive karma',NULL,NULL,NULL)
,(22,'Rewarder','bronze','Reward someone''s post with positive karma',NULL,NULL,NULL)
,(25,'Patrol','bronze','Flag a forum post which is then resolved by moderators','flags','1',6)
,(26,'Contributor','green','Submit an accepted pull request to any Defend the Web repository',NULL,NULL,NULL)
,(28,'Veteran','gold','Login after being registered for 5 years','member','1826',NULL)
,(29,'Patrol','silver','Flag 25 forum posts which is then resolved by moderators','flags','25',NULL)
,(30,'Patrol','gold','Flag 100 forum posts which is then resolved by moderators','flags','100',NULL)
,(31,'Levels','bronze','Complete all bronze levels','levels','bronze',2)
,(32,'Levels','silver','Complete all silver levels','levels','silver',3)
,(33,'Levels','gold','Complete all gold levels','levels','gold',4)
,(34,'Dark side','bronze','Go to the dark side',NULL,NULL,NULL)
,(35,'Visitor','bronze','Visit the site on 100 days','days','100',11)
,(36,'Visitor','silver','Visit the site on 500 days','days','500',NULL)
,(37,'Visitor','gold','Visit the site on 1000 days','days','1000',NULL)
,(38,'Influencer','bronze','Be followed by 5 members','followers','5',NULL)
,(41,'Influencer','silver','Be followed by 50 members','followers','50',NULL)
,(42,'Stalker','bronze','Follow 5 members','following','5',NULL)
,(43,'+ve','bronze','Have a discussion karma rating of atleast 1','karma','5',NULL)
,(44,'+ve','silver','Have a discussion karma rating of atleast 25','karma','25',NULL)
,(45,'+ve','gold','Have a discussion karma rating of atleast 100','karma','100',NULL)
;

INSERT INTO `forum_topics` VALUES (1,'News','news',NULL,'',0),(2,'Security','news-security',1,'',0),(3,'Tech','news-technology',1,'',0),(4,'Articles','articles',NULL,'',0),(5,'Ideas','articles-ideas',4,'',0),(6,'Playground','playground',NULL,'',0),(7,'Help','playground-help',6,'',0),(8,'Solutions','playground-solutions',6,'',0),(9,'Ideas','playground-ideas',6,'',0),(10,'Meta','meta',NULL,'',0),(11,'News','meta-news',10,'',0),(12,'Questions','meta-questions',10,'',0),(13,'Bugs','meta-bugs',10,'',0),(14,'Features','meta-features',10,'',0),(15,'Off-topic','offtopic',NULL,'',0),(16,'Intros','offtopic-intros',15,'',0);
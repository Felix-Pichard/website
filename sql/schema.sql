SET FOREIGN_KEY_CHECKS=0;

--
-- Table structure for table `article_revisions`
--
CREATE TABLE IF NOT EXISTS `article_revisions` (
    `revision_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `article_id` mediumint(8) UNSIGNED NOT NULL,
    `title` varchar(125) NOT NULL,
    `content` text NOT NULL,
    `status` enum('draft','review','published','declined') NOT NULL,
    `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `log` text,
    `reviewer` mediumint(8) UNSIGNED,
    `reviewed` datetime,
    PRIMARY KEY (`revision_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `article_topics`
--
CREATE TABLE IF NOT EXISTS `article_topics` (
    `topic_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` varchar(32) NOT NULL,
    `slug` varchar(128) NOT NULL,
    `order` tinyint(1) UNSIGNED DEFAULT NULL,
    `sidebar` tinyint(1) UNSIGNED DEFAULT 0,
    PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `articles`
--
CREATE TABLE IF NOT EXISTS `articles` (
    `article_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` varchar(125) NOT NULL,
    `slug` varchar(125) NOT NULL,
    `content` mediumtext NOT NULL,
    `created` datetime DEFAULT CURRENT_TIMESTAMP,
    `submitted` datetime DEFAULT CURRENT_TIMESTAMP,
    `published` datetime DEFAULT NULL,
    `updated` datetime DEFAULT NULL,
    `author` mediumint(8) UNSIGNED DEFAULT NULL,
    `topic_id` mediumint(8) UNSIGNED DEFAULT NULL,
    `status` enum('draft','review','published','declined') NOT NULL,
    `image` varchar(64) DEFAULT NULL,
    `views` mediumint(8) UNSIGNED DEFAULT '0',
    PRIMARY KEY (`article_id`),
    UNIQUE KEY `articles_PK` (`slug`),
    KEY `articles_title_IDX` (`title`),
    KEY `articles_author_IDX` (`author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `videos`
--
CREATE TABLE IF NOT EXISTS `videos` (
    `source` enum('youtube') NOT NULL,
    `id` varchar(16) NOT NULL,
    `views` mediumint(8) UNSIGNED DEFAULT '0',
    `title` text,
    `submitted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`source`, `id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `forum_flags`
--
CREATE TABLE IF NOT EXISTS `forum_flags` (
    `flag_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `post_id` mediumint(8) UNSIGNED NOT NULL,
    `user_id` mediumint(8) UNSIGNED DEFAULT NULL,
    `type` enum('newuser','late','topic','spam','quality','other') NOT NULL,
    `comment` text,
    `submitted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `resolved` tinyint(1) DEFAULT '0',
    PRIMARY KEY (`flag_id`),
    KEY `forum_flags_users_FK` (`user_id`),
    CONSTRAINT `forum_flags_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `forum_flags_moderation`
--
CREATE TABLE IF NOT EXISTS `forum_flags_moderation` (
    `flag_id` mediumint(8) UNSIGNED NOT NULL,
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `response` enum('delete','close','edit','warn','dismiss','move','skip','escalate') NOT NULL,
    `weight` tinyint(1) NOT NULL,
    `details` text,
    `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `data` blob,
    PRIMARY KEY (`flag_id`,`user_id`),
    KEY `forum_flags_moderation_users_FK` (`user_id`),
    CONSTRAINT `forum_flags_moderation_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `forum_thread_posts`
--
CREATE TABLE IF NOT EXISTS `forum_thread_posts` (
    `post_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `message` mediumtext NOT NULL,
    `author` mediumint(8) UNSIGNED DEFAULT NULL,
    `posted` datetime DEFAULT CURRENT_TIMESTAMP,
    `thread_id` mediumint(5) UNSIGNED NOT NULL,
    `updated` datetime DEFAULT NULL,
    `deleted` tinyint(1) DEFAULT '0',
    `reply_to` mediumint(8) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`post_id`),
    KEY `forum_thread_posts_forum_threads_FK` (`thread_id`),
    KEY `forum_thread_posts_users_FK` (`author`),
    CONSTRAINT `forum_thread_posts_forum_threads_FK` FOREIGN KEY (`thread_id`) REFERENCES `forum_threads` (`thread_id`),
    CONSTRAINT `forum_thread_posts_users_FK` FOREIGN KEY (`author`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `forum_threads`
--
CREATE TABLE IF NOT EXISTS `forum_threads` (
    `thread_id` mediumint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `slug` varchar(255) CHARACTER SET latin1 NOT NULL,
    `author` mediumint(8) UNSIGNED DEFAULT NULL,
    `topic_id` tinyint(3) NOT NULL DEFAULT '7',
    `sticky` tinyint(1) DEFAULT '0',
    `spoiler` tinyint(1) DEFAULT '0',
    `locked` tinyint(1) DEFAULT '0',
    `answer` mediumint(8) UNSIGNED DEFAULT NULL,
    `deleted` tinyint(1) DEFAULT '0',
    `views` mediumint(8) UNSIGNED DEFAULT '0',
    `level_id` tinyint(3) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`thread_id`),
    UNIQUE KEY `forum_threads_PK` (`slug`),
    KEY `forum_threads_forum_topics_FK` (`topic_id`),
    KEY `forum_threads_users_FK` (`author`),
    CONSTRAINT `forum_threads_forum_topics_FK` FOREIGN KEY (`topic_id`) REFERENCES `forum_topics` (`topic_id`),
    CONSTRAINT `forum_threads_users_FK` FOREIGN KEY (`author`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `forum_topics`
--
CREATE TABLE IF NOT EXISTS `forum_topics` (
    `topic_id` tinyint(2) NOT NULL AUTO_INCREMENT,
    `title` varchar(64) NOT NULL,
    `slug` varchar(64) NOT NULL,
    `parent` tinyint(2) DEFAULT NULL,
    `description` text,
    `order` tinyint(4) DEFAULT '0',
    PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `forum_karma`
--
CREATE TABLE IF NOT EXISTS `forum_karma` (
    `post_id` mediumint(8) UNSIGNED NOT NULL,
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `vote` enum('up','down') NOT NULL,
    `voted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE KEY `forum_karma_uk` (`post_id`, `user_id`),
    KEY `forum_karma_post_id` (`post_id`),
    CONSTRAINT `forum_karma_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `medals`
--
CREATE TABLE IF NOT EXISTS `medals` (
    `medal_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
    `label` varchar(16) NOT NULL,
    `colour` enum('bronze', 'silver', 'gold', 'green') CHARACTER SET latin1 NOT NULL,
    `description` text NOT NULL,
    `type` enum('levels','posts','articles','flags','avatar','member','logins','donate','todo','days','followers','following','karma') CHARACTER SET latin1,
    `count` varchar(16),
    `order` tinyint(1) DEFAULT NULL,
    PRIMARY KEY (`medal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `tickets`
--
CREATE TABLE IF NOT EXISTS `tickets` (
    `ticket_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` mediumint(8) UNSIGNED,
    `email` varchar(128),
    `created` datetime DEFAULT CURRENT_TIMESTAMP,
    `status` enum('open', 'closed', 'resolved') DEFAULT 'open',
    `token` varchar(64),
    PRIMARY KEY (`ticket_id`),
    CONSTRAINT `tickets_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `ticket_messages`
--
CREATE TABLE IF NOT EXISTS `ticket_messages` (
    `message_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
    `ticket_id` smallint(5) UNSIGNED NOT NULL,
    `content` text NOT NULL,
    `user_id` mediumint(8) UNSIGNED,
    `sent` datetime DEFAULT CURRENT_TIMESTAMP,
    `read` tinyint(1) DEFAULT 0,
    PRIMARY KEY (`message_id`),
    CONSTRAINT `ticket_messages_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `ticket_messages_ticket_FK` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `level_meta`
--
CREATE TABLE IF NOT EXISTS `level_meta` (
    `level_id` tinyint(3) UNSIGNED NOT NULL,
    `key` varchar(64) NOT NULL,
    `value` text NOT NULL,
    PRIMARY KEY (`level_id`,`key`),
    CONSTRAINT `level_meta_levels_FK` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `levels`
--
CREATE TABLE IF NOT EXISTS `levels` (
    `level_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` varchar(100) NOT NULL,
    `slug` varchar(100) NOT NULL,
    `status` enum('live','draft') NOT NULL DEFAULT 'draft',
    `order` tinyint(3) DEFAULT NULL,
    `difficulty` ENUM('bronze', 'silver', 'gold') DEFAULT 'bronze',
    `subject` varchar(100),
    PRIMARY KEY (`level_id`),
    UNIQUE KEY `levels_PK` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `pm`
--
CREATE TABLE IF NOT EXISTS pm (
        `pm_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
        PRIMARY KEY (`pm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `pm_messages`
--
CREATE TABLE IF NOT EXISTS pm_messages (
        `message_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
        `pm_id` mediumint(8) UNSIGNED NOT NULL,
        `user_id` mediumint(8) UNSIGNED,
        `message` mediumtext NOT NULL,
        `time` timestamp DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`message_id`),
        FOREIGN KEY (`pm_id`) REFERENCES pm (`pm_id`),
        FOREIGN KEY (`user_id`) REFERENCES users (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `pm_users`
--
CREATE TABLE IF NOT EXISTS pm_users (
        `pm_id` mediumint(8) UNSIGNED NOT NULL,
        `user_id` mediumint(8) UNSIGNED NOT NULL,
        `seen` timestamp NULL DEFAULT NULL,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`pm_id`, `user_id`),
        FOREIGN KEY (`pm_id`) REFERENCES pm (`pm_id`),
        FOREIGN KEY (`user_id`) REFERENCES users (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;


-- --
-- -- Table structure for table `messages`
-- --
-- CREATE TABLE IF NOT EXISTS messages (
--     `pm_id` mediumint(8) UNSIGNED NOT NULL,
--     `message_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
--     `from` mediumint(8) UNSIGNED,
--     `to` mediumint(8) UNSIGNED,
--     `message` mediumtext NOT NULL,
--     `sent` timestamp DEFAULT CURRENT_TIMESTAMP,
--     `seen` timestamp,
--     `from_deleted` tinyint(1),
--     `to_deleted` tinyint(1),
--     PRIMARY KEY (`message_id`),
--     INDEX (`pm_id`),
--     FOREIGN KEY (`from`) REFERENCES users (`user_id`),
--     FOREIGN KEY (`to`) REFERENCES users (`user_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

-- CREATE PROCEDURE messagesGetNewPMID (name VARCHAR(10))
--     BEGIN
--         DECLARE getID mediumint(8);

--         SET getID = (
--             SELECT MAX(pm_id)
--             FROM tests) + 1;

--         INSERT INTO tests (test_num, test_name)
--             VALUES (getCount, name);
--     END$$

--
-- Table structure for table `user_activity`
--
CREATE TABLE IF NOT EXISTS `user_activity` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `active` datetime DEFAULT NULL,
    `consecutive` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
    `consecutive_max` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
    `days` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
    PRIMARY KEY (`user_id`),
    CONSTRAINT `user_activity_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_auth`
--
CREATE TABLE IF NOT EXISTS `user_auth` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `service` enum('website','google','facebook','twitter','github') CHARACTER SET latin1 NOT NULL,
    `token` varchar(128) CHARACTER SET latin1 NOT NULL,
    PRIMARY KEY (`user_id`),
    KEY `users_oauth_user_id_IDX` (`user_id`),
    KEY `users_oauth_service_IDX` (`service`,`token`),
    CONSTRAINT `users_oauth_unique` UNIQUE (`service`, `token`),
    CONSTRAINT `users_oauth_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_usernames`
--
CREATE TABLE IF NOT EXISTS `user_usernames` (
    `user_id` mediumint(8) UNSIGNED,
    `username` varchar(32) NOT NULL,
    `changed` datetime DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `user_usernames_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_browsers`
--
CREATE TABLE IF NOT EXISTS `user_browsers` (
    `user_id` mediumint(8) UNSIGNED DEFAULT NULL,
    `browser_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `token` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
    `login` tinyint(1) UNSIGNED DEFAULT '0',
    `twoFactor` tinyint(1) UNSIGNED DEFAULT '0',
    PRIMARY KEY (`browser_id`),
    KEY `users_browsers_user_id_IDX` (`user_id`,`token`),
    KEY `users_browsers_token_IDX` (`token`),
    CONSTRAINT `users_browsers_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_levels`
--
CREATE TABLE    IF NOT EXISTS `user_levels` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `level_id` tinyint(3) UNSIGNED NOT NULL,
    `started` datetime DEFAULT CURRENT_TIMESTAMP,
    `finished` datetime DEFAULT NULL,
    PRIMARY KEY (`user_id`,`level_id`),
    KEY `user_levels_levels_FK` (`level_id`),
    KEY `level_id_finished` (`level_id`,`finished`),
    KEY `started` (`started`),
    CONSTRAINT `user_levels_levels_FK` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`),
    CONSTRAINT `user_levels_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_medals`
--
CREATE TABLE    IF NOT EXISTS `user_medals` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `medal_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    `awarded` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`user_id`,`medal_id`),
    CONSTRAINT `user_medals_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `user_medals_medal_FK` FOREIGN KEY (`medal_id`) REFERENCES `medals` (`medal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_notifications`
--
CREATE TABLE IF NOT EXISTS `user_notifications` (
    `notification_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `to` mediumint(8) UNSIGNED DEFAULT NULL,
    `from` mediumint(8) UNSIGNED DEFAULT NULL,
    `type` varchar(32) DEFAULT NULL,
    `data` text,
    `created` datetime DEFAULT CURRENT_TIMESTAMP,
    `read` datetime DEFAULT NULL,
    PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_profile_meta`
--
CREATE TABLE IF NOT EXISTS `user_profile_meta` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `key` varchar(64) NOT NULL,
    `value` text NOT NULL,
    PRIMARY KEY (`user_id`,`key`),
    CONSTRAINT `users_profile_meta_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_meta`
--
CREATE TABLE IF NOT EXISTS `user_meta` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `key` varchar(64) NOT NULL,
    `value` text NOT NULL,
    PRIMARY KEY (`user_id`, `key`),
    CONSTRAINT `user_meta_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_settings`
--
CREATE TABLE IF NOT EXISTS `user_settings` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `key` varchar(64) NOT NULL,
    `value` text NOT NULL,
    PRIMARY KEY (`user_id`, `key`),
    CONSTRAINT `user_settings_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_blocks`
--
CREATE TABLE IF NOT EXISTS `user_blocks` (
    `user_id` mediumint(8) UNSIGNED NOT NULL,
    `blocked_id` mediumint(8) UNSIGNED NOT NULL,
    `time` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`user_id`, `blocked_id`),
    CONSTRAINT `user_blocks_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `user_blocks_blocked_FK` FOREIGN KEY (`blocked_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_orders`
--
CREATE TABLE IF NOT EXISTS `user_orders` (
    `order_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` mediumint(8) UNSIGNED,
    `type` varchar(16) DEFAULT 'sale',
    `stripe_id` varchar(64) NOT NULL,
    `stripe_customer` varchar(64) NOT NULL,
    `order` text,
    `scalablepress` varchar(64),
    `created` datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_donations`
--
CREATE TABLE IF NOT EXISTS `user_donations` (
    `order_id` mediumint(8) UNSIGNED,
    `user_id` mediumint(8) UNSIGNED,
    `amount` smallint(3) UNSIGNED,
    `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_following`
--
CREATE TABLE IF NOT EXISTS `user_following` (
    `user_id` mediumint(8) UNSIGNED,
    `following` mediumint(8) UNSIGNED,
    `followed` datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`user_id`, `following`),
        CONSTRAINT `user_following_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
        CONSTRAINT `user_following_following` FOREIGN KEY (`following`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `user_api_keys`
--
CREATE TABLE IF NOT EXISTS `user_api_keys` (
    `user_id` mediumint(8) UNSIGNED,
    `name` varchar(32),
    `key` varchar(32),
    `created` datetime DEFAULT CURRENT_TIMESTAMP,
    `hits` int(5) DEFAULT 0,
    `accessed` datetime,
        PRIMARY KEY (`key`),
        UNIQUE KEY `key` (`key`),
        UNIQUE KEY `name` (`user_id`, `key`),
        CONSTRAINT `user_api_keys_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;

--
-- Table structure for table `users`
--
CREATE TABLE IF NOT EXISTS `users` (
    `user_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `username` varchar(32) NOT NULL,
    `password` varchar(64) DEFAULT NULL,
    `email` varchar(128) NOT NULL,
    `verified` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
    `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `privileges` enum('black', 'bronze', 'silver', 'gold', 'green'),
    `twoFactor` varchar(32) DEFAULT NULL,
    `points` smallint(5) UNSIGNED DEFAULT '0',
    `reputation` smallint(5) UNSIGNED DEFAULT '0',
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `username` (`username`),
    UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_ci;


SET FOREIGN_KEY_CHECKS=1;
-- Set points
UPDATE
    `users`,
    (
        SELECT
            `user_id`,
            SUM(
                CASE `difficulty`
                    WHEN 'bronze' THEN 10
                    WHEN 'silver' THEN 25
                    WHEN 'gold' THEN 50
                    ELSE 0 
                END
            ) as `points`
        FROM `user_levels`
        INNER JOIN `levels`
        ON `levels`.`level_id` = `user_levels`.`level_id`
        WHERE `user_levels`.`finished` IS NOT NULL
        GROUP BY `user_id`
    ) AS `src`
SET `users`.`points` = `src`.`points`
WHERE `users`.`user_id` = `src`.`user_id`;


-- Set reputatoin
UPDATE
    `users`
INNER JOIN
    (
        SELECT
            `users`.`user_id`,
            `medals`.`reputation` AS `medals`,
            `discussion_posts`.`reputation` AS `discussion_posts`,
            `discussion_threads`.`reputation` AS `discussion_threads`,
            `discussion_answers`.`reputation` AS `discussion_answers`,
            `articles`.`reputation` AS `articles`,
            (
                IFNULL(`medals`.`reputation`, 0) +
                IFNULL(`discussion_posts`.`reputation`, 0) + 
                IFNULL(`discussion_threads`.`reputation`, 0) + 
                IFNULL(`discussion_answers`.`reputation`, 0) + 
                IFNULL(`articles`.`reputation`, 0)
            ) AS `total`
        FROM `users`

        -- medals
        LEFT JOIN (
            SELECT
                `user_id`,
                SUM(
                    CASE `colour`
                        WHEN 'bronze' THEN 5
                        WHEN 'silver' THEN 15
                        WHEN 'gold' THEN 25
                        ELSE 0 
                    END
                ) as `reputation`
            FROM `user_medals`
            INNER JOIN `medals`
            ON `medals`.`medal_id` = `user_medals`.`medal_id`
            GROUP BY `user_id`
        ) `medals`
        ON `medals`.`user_id` = `users`.`user_id`

        -- articles
        LEFT JOIN (
            SELECT
                `author` AS `user_id`,
                count(*) * 25 as `reputation`
            FROM `articles`
            WHERE `status` = "published"
            GROUP BY `user_id`
        ) `articles`
        ON `articles`.`user_id` = `users`.`user_id`

        -- discussion posts
        LEFT JOIN (
            SELECT
                `author` AS `user_id`,
                FLOOR(count(*) / 10) as `reputation`
            FROM `forum_thread_posts`
            WHERE `deleted` = 0
            GROUP BY `user_id`
        ) `discussion_posts`
        ON `discussion_posts`.`user_id` = `users`.`user_id`

        -- discussion threads
        LEFT JOIN (
            SELECT
                count(*) * 10 AS `reputation`,
                `forum_threads`.`author` AS `user_id`
            FROM forum_threads
            INNER JOIN (
                SELECT `thread_id`, count(*) AS `count`
                FROM forum_thread_posts
                WHERE forum_thread_posts.deleted = 0
                GROUP BY `thread_id`
                HAVING `count` > 10
            ) `posts`
            ON `posts`.`thread_id` = `forum_threads`.`thread_id`
            WHERE forum_threads.deleted = 0
            GROUP BY `user_id`
        ) `discussion_threads`
        ON `discussion_threads`.`user_id` = `users`.`user_id`

        -- discussion answers
        LEFT JOIN (
            SELECT
                `forum_thread_posts`.`author` AS `user_id`,
                count(*) * 25 as `reputation`
            FROM `forum_thread_posts`
            INNER JOIN forum_threads
                ON forum_thread_posts.post_id = forum_threads.answer
            GROUP BY `user_id`
        ) `discussion_answers`
        ON `discussion_answers`.`user_id` = `users`.`user_id`
    ) AS `reputation`
ON `users`.`user_id` = `reputation`.`user_id`
SET `users`.`reputation` = `reputation`.`total` + `users`.`points`;


-- Update peoples privileges
UPDATE users set `privileges` = 'bronze' WHERE (`privileges` IS NULL OR `privileges` != 'green') AND `reputation` >= 300;
UPDATE users set `privileges` = 'silver' WHERE (`privileges` IS NULL OR `privileges` != 'green') AND `reputation` >= 750;
UPDATE users set `privileges` = 'gold' WHERE (`privileges` IS NULL OR `privileges` != 'green') AND `reputation` >= 1000;
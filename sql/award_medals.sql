-- carry over medals from HackThis!!
TRUNCATE TABLE user_medals;
ALTER TABLE user_medals AUTO_INCREMENT = 1;
INSERT IGNORE INTO user_medals (`user_id`, `medal_id`, `awarded`)
    SELECT `user_id`, `medal_id`, `time`
        FROM hackthis.users_medals
        WHERE `medal_id` IN (4,5,6,11,15,26);

-- award visitor medals
INSERT IGNORE INTO user_medals(`user_id`, `medal_id`)
    SELECT user_id, 35
    FROM user_activity
    WHERE `days` >= 100;

INSERT IGNORE INTO user_medals(`user_id`, `medal_id`)
    SELECT user_id, 36
    FROM user_activity
    WHERE `days` >= 500;

INSERT IGNORE INTO user_medals(`user_id`, `medal_id`)
    SELECT user_id, 37
    FROM user_activity
    WHERE `days` >= 1000;

-- award veteran medals
INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `users`.user_id, 7, `created` + INTERVAL 1 MONTH
    FROM users
    INNER JOIN user_activity
    ON user_activity.user_id = users.user_id
    WHERE `created` < NOW() - INTERVAL 1 MONTH  AND `active` > `created` + INTERVAL 1 MONTH;

INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `users`.user_id, 12, `created` + INTERVAL 1 YEAR
    FROM users
    INNER JOIN user_activity
    ON user_activity.user_id = users.user_id
    WHERE `created` < NOW() - INTERVAL 1 YEAR  AND `active` > `created` + INTERVAL 1 YEAR;

INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `users`.user_id, 28, `created` + INTERVAL 5 YEAR
    FROM users
    INNER JOIN user_activity
    ON user_activity.user_id = users.user_id
    WHERE `created` < NOW() - INTERVAL 5 YEAR  AND `active` > `created` + INTERVAL 5 YEAR;

-- award writer medal
INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `author`, 14, MIN(`published`)
    FROM `articles`
    WHERE `published` IS NOT NULL AND `status` = 'published'
    GROUP BY `author`;

-- award donator medal
INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `user_id`, 19, `created`
    FROM `user_donations`
    WHERE `user_id` IS NOT NULL AND `amount` >= 5;

-- award stalker medals
INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `user_id`,
        case 
            when post_following LIKE 5 then 42
        end as `medal_id`, IFNULL(`followed`, NOW())
        FROM (
            SELECT
                `user_id`,
                `followed`,
                @post_following := IF(@post_follower = user_id, @post_following + 1, 1) AS post_following,
                @post_follower := user_id
            FROM user_following
            ORDER BY `user_id`, `followed` ASC
        ) following
    WHERE `post_following` IN (5);

-- award level medals
INSERT IGNORE INTO user_medals(`user_id`, `medal_id`, `awarded`)
    SELECT `user_id`,
        case 
            when `difficulty` LIKE 'bronze' then 31
            when `difficulty` LIKE 'silver' then 32
            when `difficulty` LIKE 'gold' then 33
        end as `medal_id`, `latest` FROM (
            SELECT `user_id`, count(*) AS `completed`, `level`.`difficulty`, max(`finished`) as `latest`, `totals`.`total` FROM user_levels
            INNER JOIN `levels` `level`
                ON `level`.`level_id` = `user_levels`.`level_id`
            INNER JOIN (SELECT `difficulty`, count(*) AS `total` FROM `levels` GROUP BY `difficulty`) AS `totals`
                ON `totals`.`difficulty` = `level`.`difficulty`
            WHERE `finished` IS NOT NULL
            GROUP BY `difficulty`, `user_id`
            HAVING `completed` = `total`
        ) `user_completed`;

-- award karma medals
INSERT IGNORE INTO user_medals(`user_id`, `medal_id`)
    SELECT `user_id`, 43
    FROM forum_karma
    GROUP BY user_id
    HAVING SUM(case when `vote` = 'up' then 1 else -1 end) >= 5;

INSERT IGNORE INTO user_medals(`user_id`, `medal_id`)
    SELECT `user_id`, 44
    FROM forum_karma
    GROUP BY user_id
    HAVING SUM(case when `vote` = 'up' then 1 else -1 end) >= 25;

INSERT IGNORE INTO user_medals(`user_id`, `medal_id`)
    SELECT `user_id`, 45
    FROM forum_karma
    GROUP BY user_id
    HAVING SUM(case when `vote` = 'up' then 1 else -1 end) >= 100;


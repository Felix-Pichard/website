<?php
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $app->DtW->load('Articles');

        $args = array(
            'hackthis' => isset($_GET['hackthis'])
        );

        try {
            $args['articlePicks'] = \dtw\Articles::getPicks();

            $articleStats = \dtw\Articles::getStats();
            $args['articlesPublished'] = \dtw\utils\Utils::roundDown($articleStats->published);

            $args['playgroundLevelCount'] = \dtw\utils\Utils::roundDown(\dtw\Playground::getLevelCount());
            $args['playgroundLevelCompleted'] = \dtw\utils\Utils::roundDown(\dtw\Playground::getLevelsCompleted());

            $userStats = \dtw\Users::getStats();
            $args['userCount'] = \dtw\utils\Utils::roundDown($userStats->count);
            $args['userActive'] = \dtw\utils\Utils::roundDown($userStats->active);
        } catch (\Exception $e) {
            // No topics
        }

        return $app->DtW->tmpl->render('home.twig', $args);
    });
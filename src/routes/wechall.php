<?php
    $this->respond('GET', '/validateemail', function($request, $response, $service, $app) {
        $key = \dtw\DtW::$config->get('wechall');

        if (!isset($_GET['username']) ||
            !isset($_GET['email']) ||
            is_array($_GET['username']) ||
            is_array($_GET['email']) ||
            $key != $_GET['authkey']) { 
                die('0'); 
        }

        $username = $_GET['username'];
        $email = $_GET['email'];
        $stmt = \dtw\DtW::$db->prepare('
                SELECT user_id
                FROM users
                WHERE `username` = :username
                AND `email` = :email'); 
        $stmt->execute(array(
            ':username' => $username,
            ':email' => $email
        )); 
        if ($stmt->rowCount()) {
            die('1');
        }

        // Entry doesn't exist
        die('0');
    });

    $this->respond('GET', '/userscore', function($request, $response, $service, $app) {
        $key = \dtw\DtW::$config->get('wechall');

        if (!isset($_GET['username']) ||
            is_array($_GET['username']) ||
            $key != $_GET['authkey']) { 
                die('0'); 
        }

        try {
            $profile = \dtw\Users::getUser($_GET['username']);

            $stats = \dtw\Users::getStats();

            echo sprintf(
                '%s:%d:%d:%d:%d:%d:%d',
                $profile->username,
                $profile->getRank(),
                $profile->points,
                \dtw\Playground::getMaxPoints(),
                count($profile->levels),
                \dtw\Playground::getLevelCount(),
                $stats->count
            );
        } catch (\Exception $e) {
            // User doesn't exist
            die('0');
        }

    });
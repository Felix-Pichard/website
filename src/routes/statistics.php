<?php
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $args = array(
            'breadcrumb' => array(
                'Statistics' => '/statistics'
            )
        );

        $stats = array();

        // Articles
        $articles = new \dtw\Articles();
        $stats['articles'] = $articles->getStats();

        // User stats
        $stats['users'] = \dtw\Users::getStats();

        // Level stats
        $stats['levels'] = array();
        $stats['levels']['count'] = \dtw\Playground::getLevelCount();
        $stats['levels']['completed'] = \dtw\Playground::getLevelsCompleted();

        // Forum stats
        $discussions = new \dtw\Discussions();
        $stats['discussions'] = $discussions->getStats();

        $args['stats'] = $stats;

        // Leaderboard
        switch ($_GET['sort']) {
            case 'points': $sort = 'points'; break;
            default: $sort = 'reputation';
        }
        $args['sort'] = $sort;
        $args['leaderboard'] = \dtw\Statistics::getLeaderboard($sort);

        // Chat
        $args['chat'] = \dtw\Statistics::getChatStats();

        return $app->DtW->tmpl->render('statistics/index.twig', $args);
    });
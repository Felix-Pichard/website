<?php
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $args = array();

        $args['breadcrumb'] = array(
            'Dashboard' => '/dashboard'
        );

        $feed = new \dtw\utils\Feed();
        $args['discussions'] = $feed->getLatestDiscussions();
        $args['externalNews'] = $feed->getLatestExternalNews();
        $args['exploits'] = $feed->getLatestExploits();

        if ($app->DtW->user->isAuth()) {
            $args['notifications'] = $app->DtW->user->notifications->get(true);
        }

        $app->DtW->load('Articles');
        $args['articlePicks'] = $topics = \dtw\Articles::getPicks();

        return $app->DtW->tmpl->render('dashboard/index.twig', $args);
    });
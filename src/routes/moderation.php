<?php
    $this->respond('GET', '/discussions', function($request, $response, $service, $app) {
        try {
            $discussions = new \dtw\moderation\Discussions();
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $args = array();

        $args['title'] = 'Discussion queues';

        $args['breadcrumb'] = array(
            'Moderation' => '/moderation',
            'Discussions' => '/moderation/discussions'
        );

        $args['queues'] = $discussions->getQueues('discussions');

        $args['logs'] = $discussions->getResolvedLogs();

        return $app->DtW->tmpl->render('moderation/discussions_queue.twig', $args);
    });

    $this->respond('GET', '/discussions/[:queue]', function($request, $response, $service, $app) {
        try {
            $discussions = new \dtw\moderation\Discussions();
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $breadcrumb = array(
            'Moderation' => '/moderation',
            'Discussions' => '/queue/discussions'
        );

        try {
            $item = $discussions->getQueueFlag($request->queue);
            $response->redirect('/moderation/discussions/' . $request->queue . '/' . $item);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'info');
            $response->redirect('/moderation/discussions');
        }

        $response->send();
        $this->skipRemaining();
    });

    $this->respond('GET', '/discussions/[:queue]/[:flag]', function($request, $response, $service, $app) {
        try {
            $discussions = new \dtw\moderation\Discussions();
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $breadcrumb = array(
            'Moderation' => '/moderation',
            'Discussions' => '/moderation/discussions'
        );

        if (isset($_GET['action'])) {
            try {
                $item = $discussions->handleFlagAction($request->queue, $request->flag, $_GET['action']);

                // Redirect to queue
                $response->redirect('/moderation/discussions/' . $request->queue);
            } catch (Exception $e) {
                print_r($e);
                die();
                \dtw\utils\Flash::add($e->getMessage(), 'error');
                $response->redirect('/moderation/discussions/' . $request->queue . '/' . $request->flag);
            }

            $response->send();
            $this->skipRemaining();
        }

        try {
            $item = $discussions->getFlag($request->queue, $request->flag);
        } catch (Exception $e) {
            $service->refresh();

            $response->send();
            $this->skipRemaining();
        }

        $showFullThread = isset($_GET['full']);

        return $app->DtW->tmpl->render('moderation/discussion.twig', array('breadcrumb' => $breadcrumb, 'item' => $item, 'fullThread' => $showFullThread));
    });

    /*
     * Report post
     */
    $this->respond('POST', '/discussions/[:queue]/[:flag]/report', function ($request, $response, $service, $app) {
        try {
            $discussions = new \dtw\moderation\Discussions();
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $item = $discussions->getFlag($request->queue, $request->flag);

        try {
            $slug = $item['thread']->permalink;
            $slug = substr($slug, strlen('/discussions'));

            $app->DtW->discussions->report($slug, $_POST);
            \dtw\utils\Flash::add('Report submitted', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        if (isset($_POST['ajax'])) {
            $flash = $service->flashes();
            if ($flash) {
                echo json_encode($flash);
            }
        } else {
            $response->redirect('/discussion/' . $request->thread);
        }

        $response->send();
        $this->skipRemaining();

    });

    /*
     *  Articles
     */
    $this->respond('GET', '/articles', function($request, $response, $service, $app) {
        try {
            $articles = new \dtw\moderation\Articles();
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $args['title'] = 'Article queues';

        $args['breadcrumb'] = array(
            'Moderation' => '/moderation',
            'Articles' => '/moderation/articles'
        );

        $args['queues'] = $articles->getQueues();

        $args['logs'] = $articles->getLogs();

        return $app->DtW->tmpl->render('moderation/articles_queue.twig', $args);
    });

    $this->respond('GET', '/articles/new', function($request, $response, $service, $app) {
        try {
            $articles = new \dtw\moderation\Articles();
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $breadcrumb = array(
            'Moderation' => '/moderation',
            'Articles' => '/moderation/articles',
            'New submissions' => '/moderation/articles/new'
        );

        $topic = new stdClass();
        $topic->title = 'New submissions';
        $topic->articles = $articles->getNewArticles();

        return $app->DtW->tmpl->render('articles/topics.twig', array('breadcrumb' => $breadcrumb, 'topic' => $topic));
    });

    /*
     *  Tickets
     */
    $this->respond('GET', '/tickets', function($request, $response, $service, $app) {
        if (!$app->DtW->user->hasPrivilege('tickets')) {
            $this->skipRemaining();
            return;
        }

        $breadcrumb = array(
            'Admin' => '/moderation',
            'Tickets' => '/moderation/tickets'
        );

        $args = array('breadcrumb' => $breadcrumb);
        $args['tickets'] = \dtw\tickets\Tickets::getAllTickets();

        return $app->DtW->tmpl->render('moderation/tickets.twig', $args);
    });

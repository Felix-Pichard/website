<?php
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        $args = array(
            'breadcrumb' => array(
                'Settings' => '/settings',
                'API keys' => '/api'
            )
        );

        $args['keys'] = \dtw\utils\Api::getUserKeys();

        $form = new \dtw\utils\Form('Generate key', "Generate");
        $form->addField('Name', 'text');
        $args['form'] = $form;

        return $app->DtW->tmpl->render('api/index.twig', $args);
    });

    $this->respond('POST', '', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        try {
            $key = \dtw\utils\Api::generateUserKey($_POST['name']);

            \dtw\utils\Flash::add('Key generated', 'success');
            $service->back();
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $service->back();
        }
    });


    $this->respond('/1/[:module]/[:method]', function ($request, $response, $service, $app) {
        $app->DtW->load('Discussions');
        
        $validReferer = \dtw\DtW::$config->get('site.domain');
        // $requestReferer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);

        // if ($validReferer !== $requestReferer) {
        //     return;
        // }

        \dtw\DtW::$api->registerStaticHooks();

        $return = \dtw\DtW::$api->call($request->module, $request->method, $request->params());
        if ($return->status == 'error') {
            $response->code(400);
        }

        $response->json($return);
        $this->skipRemaining();
    });
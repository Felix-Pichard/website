<?php
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        $breadcrumb = array(
            'Notifications' => '/notifications'
        );

        $notifications = $app->DtW->user->notifications->get();

        return $app->DtW->tmpl->render('notifications.twig', array('breadcrumb' => $breadcrumb, 'notifications' => $notifications));
    });
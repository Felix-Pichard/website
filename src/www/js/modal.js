$('[data-modal]').on('click', function(e) {
    e.preventDefault();
    launchModal();
});

var modal = {};

modal.launch = function(title, content, callback) {
    modal.close();
    $('<div>', { class: 'overlay' }).appendTo($('body')).on('click', function(e) {
        e.preventDefault();
        modal.close();
    });
    $modal = $('<div>', { class: 'modal', html: content }).appendTo($('body'));

    if(typeof callback == 'function'){
        callback.call($modal);
    }
};

modal.feedback = function(data) {
    var $modal = $('.modal');
    $modal.height($('.modal').height());

    if (data.type == 'success') {
        $modal.addClass('feedback-success');
        $modal.html('<i class="fa fa-check-circle-o" aria-hidden="true"></i><span>' + data.msg + '</span>');
    } else {
        $modal.addClass('feedback-error');
        $modal.html('<i class="fa fa-times-circle-o" aria-hidden="true"></i><span>' + data.msg + '</span>');
    }
};

modal.close = function() {
    $('.overlay').remove();
    $('.modal').remove();
};
$(function() {
    function setHeights() {
        var height = $('.auth-local > .active').height();
        $('.auth-local').height(height);
    }

    $('.auth-tabs li').on('click', function(e) {
        e.preventDefault();

        // $('.auth-local').addClass('auth-local--animated');

        $(this).addClass('active');
        $(this).siblings().removeClass('active');

        // Show correct tab
        $('.auth-local > .active').removeClass('active');
        $('.auth-local').children('.'+$(this).data('target')).addClass('active');

        setHeights();

        var url = '/auth';
        if ($(this).data('target') == 'auth-local-signup') {
            url += '/signup';
        }
        window.history.pushState({}, "", url);
    });

    $('[type=email]').on('blur', function() {
        var $this = $(this);
        $this.siblings('.email-suggestion').remove();

        $this.mailcheck({
            suggested: function(element, suggestion) {
                $('<a>', { text: 'Did you mean ' + suggestion.full + '?', href : '#', class: 'email-suggestion right small' }).on('click', function(e) {
                    e.preventDefault();
                    $(this).siblings('[type=email]').val(suggestion.full);
                    $(this).remove();
                }).insertAfter($this);
            }
        });
    });
});
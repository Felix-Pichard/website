$(function() {

    var page = 1,
        topic,
        order,
        loading = true;

    $('.pagination .button').on('click', function(e) {
        e.preventDefault();
        loading = false;
        page = $(this).data('page');
        topic = $(this).data('topic');
        order = $(this).data('order');
        loadMoreThreads();
        $(this).hide();
    });

    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() + 200 >= $(document).height()) {
           loadMoreThreads();
        }
    });

    function loadMoreThreads() {
        if (loading) {
            return;
        }

        page++;
        loading = true;
        $('.pagination .loader').removeClass('hide');

        $.get('/discussions/' + topic + '/' + order + '/' + page, { ajax: true }, function(data) {

            $('.discussion-list-threads:not(.discussion-list-threads--sticky)').append(data);

            loading = false;
        }).always(function() {
            $('.pagination .loader').addClass('hide');
        });
    }



    var flagTemplate = '<form method="post">\
            <input type="hidden" name="post_id" value="">\
            <ul class="discussions-report">\
                <li>\
                    <label>\
                        <input type="radio" name="reason" value="topic"/>\
                        It is off-topic\
                    </label>\
                    This post is not useful or relevant to the current thread, or the thread itself is not appropriate to this section.\
                </li>\
                <li>\
                    <label>\
                        <input type="radio" name="reason" value="spam"/>\
                        It is spam\
                    </label>\
                    This post is effectively an advertisement with no disclosure. It is not useful or relevant, but promotional.\
                </li>\
                <li>\
                    <label>\
                        <input type="radio" name="reason" value="quality"/>\
                        It is very low quality\
                    </label>\
                    This post has severe formatting or content problems. The post is unlikely to be salvageable through editing.\
                </li>\
                <li class="input">\
                    <label>\
                        <input type="radio" name="reason" value="other"/>\
                        Other\
                    </label>\
                    This post needs a moderator\'s attention. Please describe exactly what\'s wrong.\
                    <div class="discussions-report-other">\
                        <textarea name="comment" placeholder="Explain reason"/>\
                    </div>\
                </li>\
            </ul>\
            <input type="submit" class="button button--main button--wide" value="Report post"/>\
        </form>'

    $('[data-report]').on('click', function(e) {
        e.preventDefault();

        var post_id = $(this).closest('.discussion-thread-message').data('post-id');

        modal.launch('Report post', flagTemplate, function() {
            var $modal = $(this);

            $modal.find('form').attr('action', location.pathname + '/report');
            $modal.find('input[name="post_id"]').val(post_id);

            $modal.find('input[type=radio]').on('change', function() {
                if ($modal.find('input[type=radio][value=other]:checked').length) {
                    $modal.find('.discussions-report-other').slideDown('fast');
                } else {
                    $modal.find('.discussions-report-other').slideUp('fast');
                }
            });

            $modal.find('input[type=submit]').on('click', function(e) {
                e.preventDefault();

                $.ajax({
                    url: location.pathname + '/report',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        ajax: true,
                        post_id: post_id,
                        reason: $modal.find('input:checked').val(),
                        comment: $modal.find('textarea').val()
                    },
                    success: function(data) {
                        modal.feedback(data);
                    }
                });
            });
        });
    });


    // Img lightbox
    if (typeof lightbox !== 'undefined') {
        $('.discussion-thread-message-content img:not(.emoji)').each(function() {
            $(this).wrap($('<a>', { 'href': $(this).attr('src'), 'data-lightbox': 'discussion-images'}));
        });
        lightbox.option({
            'resizeDuration': 0,
            'fadeDuration': 0,
            'imageFadeDuration': 0,
            'wrapAround': false,
            'positionFromTop': 50
        })
        lightbox.init();

        $('.discussion-thread-info-images').on('click', function(e) {
            e.preventDefault();

            var $target = $('a[data-lightbox=discussion-images]:eq(0)');
            lightbox.start($target);
        });
    }

    // Delete post
    $('[data-delete]').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        $(this)
            .closest('.discussion-thread-message')
            .addClass('iscussion-thread-message--deleted block--clean')
            .html('<em>Reply has been removed</em>');

        $.post($(this).attr('href'));
    });

    // Reply
    $('[data-reply]').on('click', function(e) {
        e.preventDefault();

        var post_id = $(this).closest('.discussion-thread-message').data('post-id');

        replyTo(post_id);
        goto();
    });

    function goto(target) {
        var top = $(document).height();

        if (target) {
            top = $(target).offset().top - 100;

            $('.highlight').removeClass('highlight');
            $(target).addClass('highlight');
        }

        $("html, body").animate({ scrollTop: top }, 200);
    }


    $(location.hash).addClass('highlight');
    $("a[href^=#post-]").on('click', function(e) {
        e.preventDefault();
        var targetID = $(this).attr('href');
        goto(targetID);
    });

    function replyTo(post_id) {
        // Remove any existing reply targets
        $('.discussion-thread-reply-parent').remove();

        var $parent = $('.discussion-thread-message[data-post-id=' + post_id + ']').clone();
        $parent.removeClass('highlight discussion-thread-message--root');
        $parent.find('.discussion-thread-message-meta .right').remove();

        $parent.append($('<input>', { type: 'hidden', value: post_id, name: 'replyTo' }));

        $parentContainer = $('<div>', { class: "discussion-thread-reply-parent" });
        $('<strong>', { text: 'Replying to' }).appendTo($parentContainer);
        $parent.appendTo($parentContainer);

        $('.discussion-thread-reply .markdown-editor').before($parentContainer);
    }


    // Scroll indicator
    if ($('.discussion-scroll-indicator').length) {
        var $messages = $('.discussion-thread-message:not(.discussion-thread-message--deleted)'),
            $count = $('.discussion-scroll-indicator span');

        $(window).scroll(function() {
            var closest = 1;
            
            var docView = $(window).scrollTop() + ($(window).height() * 0.7);
            $messages.each(function(index) {
                var elemTop = $(this).offset().top;
                if (elemTop < docView) {
                    closest = index + 1;
                }
            });

            $count.text(closest);
        });
    }


    // Karma
    // Get currently awarded karma
    if ($('.discussion-thread-message-karma a[data-karma]').length) {
        var threadID = $('.discussion-thread').data('thread-id');

        $.post(document.dtw.api + 'discussions/awardedKarma', {
            thread: threadID
        }, function(data) {
            var votes = data.data;

            for (var key in votes) {
                if (votes.hasOwnProperty(key)) {
                    $('.discussion-thread-message[data-post-id='+key+'] .discussion-thread-message-karma').addClass('discussion-thread-message-karma--' + votes[key]);
                }
            }
        });
    }

    $('.discussion-thread-message-karma a[data-karma]').on('click', function(e) {
        e.preventDefault();

        var $this = $(this),
            $parent = $this.parent(),
            currentValue = parseInt($this.siblings('h3').text()),
            newValue = currentValue,
            previousAction = $parent.hasClass('discussion-thread-message-karma--up') ? 'up' : ($parent.hasClass('discussion-thread-message-karma--down') ? 'down' : null);
            action = $this.data('karma');

        if (action === 'up') {
            if (previousAction === 'up') {
                return;
            }

            newValue++;
            if (previousAction === 'down') {
                newValue++;
            }

            $this.siblings('h3').text(newValue);

            $parent.removeClass('discussion-thread-message-karma--down');
            $parent.addClass('discussion-thread-message-karma--up');
        } else if (action === 'down') {
            if (previousAction === 'down') {
                return;
            }

            newValue--;
            if (previousAction === 'up') {
                newValue--;
            }

            $this.siblings('h3').text(newValue);

            $parent.removeClass('discussion-thread-message-karma--up');
            $parent.addClass('discussion-thread-message-karma--down');
        } else {
            return;
        }

        $this.addClass('active');

        $.ajax({
            url: $(this).attr('href'),
            dataType: 'json',
            type: 'POST',
            data: {
                ajax: true
            },
            success: function(data) {
                // All good
            },
            error: function(data) {
                // reset
                $this.siblings('h3').text(currentValue);

                $parent.removeClass('discussion-thread-message-karma--' + action);
                if (previousAction) {
                    $parent.addClass('discussion-thread-message-karma--' + previousAction);
                }
            }
        });
        
    });



    // New topic
    var $topic = $('select#topic');
    if ($topic.length) {
        showHideNewField($topic.val());
        $topic.on('change', function() {
            showHideNewField($topic.val());
        });
    }

    var levelCache;
    function showHideNewField(topic) {
        if (!levelCache) {
            levelCache = [];
            $('div[data-group=level] option').each(function() {
                levelCache.push([$(this).val(), $(this).text()]);
            });
        }

        $('div[data-group=level]').hide();
        $('div[data-group=spoilers]').hide();
        if (topic == 7) {
            $('div[data-group=level]').show();
            $('div[data-group=spoilers]').show();

            // Remove incomplete levels
            $('div[data-group=level] option').remove();
            $('div[data-group=level] select').append($('<option>', { value: 0, text: '---'}));
            $.each(levelCache, function() {
                if (!levelsCompleted.includes(this[0])) {
                    $('div[data-group=level] select').append($('<option>', { value: this[0], text: this[1]}));
                }
            });
        } else if (topic == 8) {
            $('div[data-group=level]').show();

            // Remove complete levels
            $('div[data-group=level] option').remove();
            $.each(levelCache, function() {
                if (levelsCompleted.includes(this[0])) {
                    $('div[data-group=level] select').append($('<option>', { value: this[0], text: this[1]}));
                }
            });
        }
    }

});
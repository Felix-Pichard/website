// This weeks stats
var chatStatsWeekData = [],
    chatStatsWeekLabels = [],
    days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
for(var i = 6; i >= 0; i--) {
    var date = new Date();
    if (i == 0) {
        chatStatsWeekLabels.push('Today');
    } else {
        date.setDate(date.getDate() - i);

        chatStatsWeekLabels.push(days[date.getDay()]);
    }

    date.setUTCHours(0,0,0,0);

    console.log(date.toISOString());

    var lines = chatStats.days[date.toISOString()];
    chatStatsWeekData[6 - i] = lines ? lines : 0;
}


var ctx = document.getElementById("chatStatsWeek").getContext('2d');
var chatStatsWeek = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: chatStatsWeekLabels,
        datasets: [{
            label: 'Lines',
            backgroundColor: 'green',
            hoverBackgroundColor: 'green',
            data: chatStatsWeekData,
            borderWidth: 0
        }]
    },
    options: {
        aspectRatio: 3.5,
        title: {
            display: true,
            fontStyle: 'normal',
            fontSize: 14,
            text: 'Activity over the last 7 days'
        },
        animation: {
            duration: 0
        },
        tooltips: {
            enabled: false
        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                position: 'left',
                gridLines: {
                    display: false,
                    drawBorder: true
                },
                ticks: {
                    beginAtZero:true,
                    maxTicksLimit: 5
                },
            }],
            xAxes: [{
                categoryPercentage: 0.9,
                gridLines: {
                    display: false,
                    drawBorder: true
                },
                ticks: {
                    beginAtZero:true,
                    padding: 6
                },
            }],
        }
    }
});


// All time hourly stats
var chatStatsHoursData = [],
    chatStatsHoursBg = [];
for(var i = 0; i <= 23; i++) {
    var colour = '#ff2400';
    if (i <= 5) {
        colour = '#1e1e1e';
    } else if (i <= 11) {
        colour = '#fe7518';
    } else if (i <= 17) {
        colour = 'green';
    }
    chatStatsHoursBg[i] = colour;
    chatStatsHoursData[i] = chatStats.hours[i] ? chatStats.hours[i] : 0;
}


var ctx = document.getElementById("chatStatsHours").getContext('2d');
var chatStatsHours = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: Array.from(Array(24).keys()),
        datasets: [{
            label: 'Lines',
            backgroundColor: chatStatsHoursBg,
            hoverBackgroundColor: chatStatsHoursBg,
            data: chatStatsHoursData,
            borderWidth: 0
        }]
    },
    options: {
        aspectRatio: 3.5,
        title: {
            display: true,
            fontStyle: 'normal',
            fontSize: 14,
            text: 'Most active hours (GMT)'
        },
        animation: {
            duration: 0
        },
        tooltips: {
            enabled: false
        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                position: 'left',
                gridLines: {
                    display: false,
                    drawBorder: true
                },
                ticks: {
                    beginAtZero:true,
                    maxTicksLimit: 5
                },
            }],
            xAxes: [{
                categoryPercentage: 0.9,
                gridLines: {
                    display: false,
                    drawBorder: true
                },
                ticks: {
                    beginAtZero:true
                },
            }],
        }
    }
});
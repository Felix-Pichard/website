### Why do so many users are following exactly the same number of people following them?
Followers is the second generation of our friendship system. Originally members had to agree to be friends with another user who requested it. Members in approved friendships both got marked as following each other during the migration. Friendships that had not been accepted were left as only the requester following the other account.

### I don't want people to see information about me!
All fields on the [edit profile](/settings/profile) page are optional. By not filling them in and marking all extra items as hidden your profile will only display basic information about your account. These will include your username, reputation and playground progress.

### What is reputation?
See the [reputation help page](/help/reputation-and-privileges/what-is-reputation).
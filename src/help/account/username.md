Your username is a unique handle identifying your account. Changing it can have unintended side effects.

* Users will not be able to find you if they do not know your new username
* Mention links will no longer link to your account
* After a grace period other users will be able to register your previous username
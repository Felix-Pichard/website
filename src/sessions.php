<?php
    // Create sessions folder if required
    $sessionPath = realpath(dirname($_SERVER['DOCUMENT_ROOT'], 2) . '/sessions');
    if (!file_exists($sessionPath)) {
        mkdir($sessionPath, 0770, true);
    }
    ini_set('session.save_path', $sessionPath);
    session_start();
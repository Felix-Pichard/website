<?php
    namespace dtw\tickets;

    class Ticket {

        private $_ticket;

        public function __construct($ticketID, $force = false) {
            if ($force) {
                $this->id = $ticketID;
                return;
            }

            // Lookup ticket
            $stmt = \dtw\DtW::$db->prepare('SELECT * FROM tickets WHERE `ticket_id` = :ticketID'); 
            $stmt->execute(array(':ticketID' => $ticketID)); 
            if ($stmt->rowCount()) {
                $this->_ticket = $stmt->fetch();
                $this->id = $this->_ticket->ticket_id;
            } else {
                throw new \Exception('Ticket not found');
            }

            // Check user has access
            $DtW = \dtw\DtW::getInstance();

            $this->owner = false;
            $this->access = false;
            if (($this->_ticket->user_id && $this->_ticket->user_id == $DtW->user->id) ||
                ($_GET['token'] && $_GET['token'] == $this->_ticket->token)
            ) {
                $this->owner = true;
                $this->access = true;
            } else if ($DtW->user->hasPrivilege('tickets')) {
                $this->access = true;
            }


            if ($this->access) {
                $this->permalink = '/help/contact/' . $this->id;

                $this->loadMessages();
            } else {
                throw new \Exception('Ticket not found');
            }
        }

        private function loadMessages() {
            $stmt = \dtw\DtW::$db->prepare('SELECT * FROM ticket_messages WHERE `ticket_id` = :ticketID ORDER BY `sent` ASC'); 
            $stmt->execute(array(':ticketID' => $this->id)); 
            if ($stmt->rowCount()) {
                $this->_ticket->messages = $stmt->fetchAll();

                foreach($this->_ticket->messages AS &$message) {
                    $content = $message->content;
                    $message->content = new \stdClass();
                    $message->content->plain = $content;
                    $message->content->safe = htmlspecialchars($content);
                    $message->content->html = \dtw\utils\Markdown::parse($content, true);

                    $message->owner = ($this->_ticket->user_id && $this->_ticket->user_id == $message->user_id);
                }
            } else {
                throw new \Exception('Ticket not found');
            }
        }

        public function addMessage($message) {
            if (!$message) {
                throw new \Exception('Message missing');
            }

            $DtW = \dtw\DtW::getInstance();

            $userID = null;
            if ($DtW->user->isAuth()) {
                $userID = $DtW->user->id;
            }

            try {
                $stmt = \dtw\DtW::$db->prepare('INSERT INTO ticket_messages (`ticket_id`, `user_id`, `content`, `read`) VALUES (:ticketID, :userID, :content, :read)');
                $stmt->execute(array(
                    ':ticketID' => $this->id,
                    ':userID' => $userID,
                    ':content' => $message,
                    ':read' => $this->owner ? 1 : 0
                ));
            } catch (\Exception $e) {
                throw new \Exception('Error creating message');
            }

            try {
                $ticketData = (object) array(
                    'ticket' => $this->id
                );
                if (!$this->owner) {
                    \dtw\Notifications::send($this->_ticket->user_id, 'ticket.reply', $ticketData, true);
                } else {
                    // Get moderators envolved in this ticket
                    $stmt = \dtw\DtW::$db->prepare('
                        SELECT `user_id`
                        FROM ticket_messages
                        WHERE `ticket_id` = :ticketID
                            AND `user_id` != :owner
                        GROUP BY `user_id`
                    '); 
                    $stmt->execute(array(
                        ':ticketID' => $this->id,
                        ':owner' => $this->_ticket->user_id
                    )); 
                    if ($stmt->rowCount()) {
                        $voices = $stmt->fetchAll(\PDO::FETCH_COLUMN);

                        foreach($voices AS $voice) {
                            \dtw\Notifications::send($voice, 'ticket.reply', $ticketData, false);
                        }
                    }
                }
            } catch (\Exception $e) {
                // Notifications not sent
            }
        }

        public function markAsRead() {
            // Mark all messages as read if OP
            if ($this->owner) {
                $stmt = \dtw\DtW::$db->prepare('UPDATE `ticket_messages` SET `read` = 1 WHERE `ticket_id` = :ticketID'); 
                $stmt->execute(array(
                    ':ticketID' => $this->id
                ));
            }
        }

        public function setStatus($status) {
            $statuses = array('open', 'closed', 'resolved');

            if (!in_array($status, $statuses)) {
                throw new \Exception("Invalid status type");
            }

            $stmt = \dtw\DtW::$db->prepare('UPDATE `tickets` SET `status` = :status WHERE `ticket_id` = :ticketID'); 
            $stmt->execute(array(
                ':ticketID' => $this->id,
                ':status' => $status
            ));

            $ticketData = (object) array(
                'ticket' => $this->id,
                'status' => $status
            );
            if (!$this->owner) {
                \dtw\Notifications::send($this->_ticket->user_id, 'ticket.status', $ticketData);
            }
        }

        public function __get($name) {
            if ($this->_ticket && property_exists($this->_ticket, $name)) {
                return $this->_ticket->$name;
            }
        }

        public function __isset($name) {
            return property_exists($this->_ticket, $name);
        }

    }
<?php
    namespace dtw;

    class Help {

        public static function getIndex() {
            $root = realpath(dirname($_SERVER['DOCUMENT_ROOT'], 1) . '/help');
            $index = array();

            $topics = scandir($root);
            $topics = array_diff($topics, array('.', '..'));

            foreach($topics AS $topic) {
                if (is_dir($root . '/' . $topic)) {
                    $title = utils\Utils::unslug($topic);
                    $index[$title] = array();

                    foreach (glob("{$root}/{$topic}/*.md") as $filename) {
                        $p = basename($filename);
                        $page = new \stdClass();
                        $page->file = $filename;
                        $page->slug = utils\Utils::slug(basename($p, '.md'));
                        $page->permalink = '/help/' . $topic . '/' . $page->slug;
                        $page->title = utils\Utils::unslug(basename($p, '.md'));

                        array_push($index[$title], $page);
                    }
                }
            }

            // Add extra items
            $contact = new \stdClass();
            $contact->title = "Contact us about anything";
            $contact->permalink = '/help/contact';
            array_unshift($index['Overview'], $contact);

            $contact = new \stdClass();
            $contact->title = "View a full list of medals";
            $contact->permalink = '/help/medals';
            array_unshift($index['Medals'], $contact);

            // Move overview to the first item
            $index = array('Overview' => $index['Overview']) + $index;

            return $index;
        }

        public static function getContent($topic, $slug) {
            $index = self::getIndex();

            $topic = utils\Utils::unslug($topic);
            if (!isset($index[$topic])) {
                throw new \Exception('Topic missing');
            }

            $section = $index[$topic];

            $page = null;
            foreach($section as $p) {
                if ($slug == $p->slug) {
                    $page = $p;
                    break;
                }
            }

            if (!$page) {
                throw new \Exception('Content missing');
            }

            if (!file_exists($page->file)) {
                throw new \Exception('File missing');
            }

            $page->topic = $topic;
            $content = file_get_contents($page->file);
            $page->content = \dtw\utils\Markdown::parse($content, true);

            if ($page->slug == 'code-of-conduct') {
                \dtw\user\Meta::set('view:help:conduct', true, false);
            }

            return $page;
        }

    } 
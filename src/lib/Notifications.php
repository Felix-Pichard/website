<?php
    namespace dtw;

    class Notifications {

        public static function send($to, $type, $data = null, $from = null, $email = true) {
            $notification = new Notification();

            $notification->to = $to;
            $notification->type = $type;
            $notification->data = $data;
            $notification->from = $from;
            $notification->created = time();

            $notification->save($email);
        }

        public static function delete($to, $type, $data) {
            $stmt = \dtw\DtW::$db->prepare("
                DELETE FROM `user_notifications` 
                WHERE `to` = :to AND `type` = :type AND `data` = :data;
            ");
            $stmt->execute(array(
                ':to' => $to,
                ':type' => $type,
                ':data' => json_encode($data)
            ));
        }

    }
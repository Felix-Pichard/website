<?php
    namespace dtw\utils;

    /**
    * A reliable Redis queue
    *
    * Allows for item to be queued for later processing. Items are added to a specific
    * queue. Later a worker can retrieve items FIFO. Items are stored in a processing
    * queue until the script confirms the item has finished processing.
    */

    class Queue {
        /**
        * Add item to queue. Non-string data will be serialized
        *
        * @param string $queue queue name
        * @param mixed $item item data
        */
        public static function add($queue, $item) {
            $queue = 'queue:' . $queue;

            $item = serialize($item);
            \dtw\DtW::$redis->rpush($queue, $item);
        }

        /**
        * Retrieve oldest item from queue
        *
        * @param string $queue queue name
        * @return string raw item data, may be serialized
        */
        public static function next($queue) {
            $queue = 'queue:' . $queue;
            $processingQueue = $queue . ':processing';

            $item = \dtw\DtW::$redis->rpoplpush($queue, $processingQueue);

            return unserialize($item);
        }

        /**
        * Confirm item has been processed correctly to remove it from processing queue
        *
        * @param string $queue queue name
        * @param string $item raw item data
        */
        public static function done($queue, $item) {
            $queue = 'queue:' . $queue;
            $processingQueue = $queue . ':processing';

            $item = serialize($item);

            \dtw\DtW::$redis->lrem($processingQueue, 1, $item);
        }

        /**
         * Get number of items in a specific queue
         * 
         * @param string $queue queue name
         * @param boolean $processing return length of processing queue
         * @return integer number of items in queue
         */
        public static function count($queue, $processing = false) {
            $queue = 'queue:' . $queue;
            if ($processing) {
                $queue .= ':processing';
            }

            return \dtw\DtW::$redis->llen($queue);
        }

    }
<?php
    namespace dtw\utils;

    abstract class DataObject {
        protected $data;
        protected $type;
        protected $table;

        public function __construct($lookup = null) {
            if ($lookup !== null) {
                try {
                    if ($lookup && ctype_digit(strval($lookup))) {
                        $this->getByID($lookup);
                    } else if ($lookup) {
                        $this->getBySlug($lookup);
                    }
                } catch (\Exception $e) {
                    throw $e;
                }
            }
        }

        protected function updateCache($previousSlug = null) {
            // Store in Redis
            \dtw\DtW::$redis->set($this->type . ':' . $this->id, json_encode($this->data));

            // Set expiry
            \dtw\DtW::$redis->expire($this->type . ':' . $this->id, 900);

            if ($previousSlug !== null) {
                // Delete old Redis slug lookup, in case slug has changed
                \dtw\DtW::$redis->del($this->type . ':slug:' . $previousSlug);

                \dtw\DtW::$log->info('article.update', array('title' => $this->data->title, 'deleting' => 'sidebar'));

                // Delete sidebar content
                \dtw\DtW::$redis->del('sidebar');
            }

            // Create Redis slug lookup
            if (isset($this->data->slug)) {
                \dtw\DtW::$redis->set($this->type . ':slug:' . $this->data->slug, $this->id);
            }

            \dtw\DtW::$redis->del($this->type . 's');
        }

        protected function generateSlug($title, $previousSlug = null, $field = 'article_id', $iteration = 0) {
            $slug = \dtw\utils\Utils::slug($title);
            if ($iteration > 0) {
                $slug .= '-' . $iteration;
            }

            if ($slug !== $previousSlug) {
                // Does slug already exist?
                $stmt = \dtw\DtW::$db->prepare("SELECT {$field} FROM {$this->table} WHERE `slug` = :slug");
                $stmt->execute(array(':slug' => $slug)); 
                if ($stmt->rowCount()) {
                    return $this->generateSlug($title, $previousSlug, $field, ++$iteration);
                }
            }

            return $slug;
        }

        public function __get($name) {
            if ($this->data && property_exists($this->data, $name)) {
                return $this->data->$name;
            }
        }

        public function __isset($name) {
            return property_exists($this->data, $name);
        }

        public function getData() {
            return $this->data;
        }

        public function countView() {
            if (!$this->data->viewsCached) {
                $this->data->viewsCached = array();
            }

            $identifier = md5($_SERVER['REMOTE_ADDR']);
            $key = $this->type . ':' . $this->data->id . ':views';
            if (!\dtw\DtW::$redis->sIsMember($key, $identifier)) {
                \dtw\DtW::$redis->sAdd($key, $identifier);
            }

            \dtw\DtW::$redis->sAdd($this->type . 's:updated', $this->data->id);
        }
    }
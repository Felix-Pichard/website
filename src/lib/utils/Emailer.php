<?php
    namespace dtw\utils;

    /**
    * Emailer queues emails for sending
    */

    class Emailer {
        /**
        * Add email to queue.
        *
        * @param mixed $to Email address or user ID
        * @param string $subject Email subject
        * @param string $template Template name
        * @param string $data Template data
        */
        public static function send($to, $subject, $template, $data) {
            $dtw = \dtw\DtW::getInstance();

            // Check if template exists
            if (!file_exists('../templates/emails/' . $template . '.twig')) {
                throw new \Exception('Email template doesn\'t exist');
            }

            if ($to == null) {
                $to = $dtw->user->id;
            }

            if (!is_array($data)) {
                $data = (array) $data;
            }

            // Get users email if user ID given
            if ($to) {
                $u = new \dtw\user\Profile($to);

                $stmt = \dtw\DtW::$db->prepare('
                    SELECT email
                    FROM users
                    WHERE users.`user_id` = :id'); 
                $stmt->execute(array(':id' => $u->id)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    $to = $row->email;
                } else {
                    return;
                }

                if ($template !== 'password-reset') {
                    $token = \dtw\utils\Utils::generateToken();
                    \dtw\DtW::$redis->zAdd('auth:token:' . $u->id, time(), $token);

                    $data['unsubscribe'] = sprintf('%s/unsubscribe?id=%s&token=%s', \dtw\DtW::$config->get('site.domain'), $u->id, $token);
                }

                $data['username'] = $u->username;
            }

            $body = $dtw->tmpl->render('emails/' . $template . '.twig', $data);

            $email = new \stdClass();
            $email->to = $to;
            $email->subject = $subject;
            $email->body = $body;

            Queue::add('emails', $email);
        }

        public static function process() {
            $limit = 25;

            $n = 0;
            do {
                $item = Queue::next('emails');

                if ($item) {
                    self::dispatch($item);
                    $n++;
                }
            } while ($item && $n < $limit);

            return $n;
        }

        private static function dispatch($item) {
            $mail = new \PHPMailer\PHPMailer\PHPMailer();

            $config = \dtw\DtW::$config->get('smtp');
            if ($config) {
                $mail->isSMTP();
                $mail->Host = $config['server'];
                $mail->SMTPAuth = true;
                $mail->Username = $config['username'];
                $mail->Password = $config['password'];
                if ($config['ssl']) {
                    $mail->SMTPSecure = 'tls';
                }
                $mail->Port = $config['post'] ?: 587;
            }

            $mail->From = "no-reply@defendtheweb.net";
            $mail->FromName = "Defend the Web";
            $mail->addAddress($item->to);
            $mail->isHTML(true);

            $mail->Subject = $item->subject;
            $mail->Body = $item->body;

            if(!$mail->send()) {
                // echo "Mailer Error: " . $mail->ErrorInfo;
                \dtw\DtW::$log->error('emailer.send', array('to' => $item->to, 'subject' => $item->subject, 'error' => $mail->ErrorInfo));
            } else {
                // Remove item from processing queue
                \dtw\DtW::$log->info('emailer.send', array('to' => $item->to, 'subject' => $item->subject));
                Queue::done('emails', $item);
            }
        }
    }
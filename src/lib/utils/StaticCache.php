<?php
    namespace dtw\utils;

    class StaticCache {
        private static $folder = __DIR__ . '/../../staticCache/';
        private static $active;

        public static function isActive() {
            if (!isset(static::$active)) {
                static::$active = (
                    !isset($_SESSION['user']) &&
                    !count($_REQUEST) &&
                    $_SERVER['REQUEST_URI'] &&
                    substr($_SERVER['REQUEST_URI'], 0, strlen('/auth')) != '/auth' &&
                    !isset($_SESSION['flashes'])
                );
            }

            return static::$active;
        }

        public static function load() {
            if (static::isActive()) {
                $hash = md5($_SERVER['REQUEST_URI']);

                $cache = static::$folder . $hash;

                if (file_exists($cache)) {
                    if (time() - filemtime($cache) > 60) {
                        // clear cache
                        \dtw\utils\StaticCache::delete($_SERVER['REQUEST_URI']);
                    } else {
                        echo file_get_contents($cache);
                        die();
                    }
                }
            }
        }

        public static function save($content) {
            // Does cache folder exist?
            if (!file_exists(static::$folder)) {
                mkdir(static::$folder);
            }

            if (!trim($content)) {
                return;
            }

            // Is there a form on this page?
            if (strpos($content, '<form') !== false) {
                return;
            }

            if (static::isActive()) {
                $hash = md5($_SERVER['REQUEST_URI']);
                $cache = static::$folder . $hash;
                $body = $content;
                $body .= "\n\n<!-- Cache created: " . date('r') . " -->";
                file_put_contents($cache, $body);
            }
        }

        public static function delete($path) {
            $hash = md5($path);
            $cache = static::$folder . $hash;

            if (file_exists($cache)) {
                unlink($cache);
            }
        }

        public static function clearAll() {
            array_map('unlink', glob(static::$folder . "/*"));
        }

    }

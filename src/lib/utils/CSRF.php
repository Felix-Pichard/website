<?php
    namespace dtw\utils;
    require_once $_SERVER['DOCUMENT_ROOT'] . '/../sessions.php';

    class CSRF {
        public static function check($provided) {
            if (!isset($_SESSION['csrf'])) {
                return false;
            }

            // Remove expired
            foreach ($_SESSION['csrf'] AS $token => $time) {
                if (time() >= $time) {
                    unset($_SESSION['csrf'][$token]);
                }
            }

            // Check for this token
            if (!isset($_SESSION['csrf'][$provided])) {
                return false;
            }

            // Delete token
            // unset($_SESSION['csrf'][$provided]);

            return true;
        }

        public static function generate() {
            if (!isset($_SESSION['csrf'])) {
                $_SESSION['csrf'] = [];
            }

            $token = substr(bin2hex(openssl_random_pseudo_bytes(64)), 0, 64);

            $expires = time() + 900; // 15 minutes

            $_SESSION['csrf'][$token] = $expires;

            return $token;
        }

    }
<?php
    namespace dtw;

    class Images {
        private $sizes = array(
            'full' => array(2000),
            'lazy' => array(3, 3),
            'small' => array(300),
            'medium' => array(650),
            'large' => array(1000),
            'navatar' => array(40, 40),
            'avatar' => array(80, 80),
            'bigavatar' => array(250, 250)
        );

        public function __construct() {
            $this->uploadDir = realpath(dirname($_SERVER['DOCUMENT_ROOT'], 2) . '/uploads') . '/';
            $this->outputDir = realpath($_SERVER['DOCUMENT_ROOT'] . '/imgs/uploads/') . '/';

            $this->manager = new \Intervention\Image\ImageManager(array('driver' => 'imagick'));

            if (!file_exists($this->uploadDir)) {
                mkdir($this->uploadDir, 0770, true);
            }
            if (!file_exists($this->outputDir)) {
                mkdir($this->outputDir, 0770, true);
            }
        }

        static public function registerHooks() {
            \dtw\DtW::$api->registerHook('images', 'upload', function($params) {
                $DtW = \dtw\DtW::getInstance();
                $DtW->load('images');

                if (file_exists($_FILES['upload']['tmp_name']) && is_uploaded_file($_FILES['upload']['tmp_name'])) {
                    $image = $DtW->images->upload($_FILES['upload']);
                    $response = new \stdClass();
                    $response->file = $image;
                    $response->url = $DtW->images->get($image, 'medium');
                    return $response;
                }

                return false;
            });
        }

        public function get($file, $size, $lazy=false) {
            if (!array_key_exists($size, $this->sizes)) {
                throw new \Exception('Invalid size');
            }

            $file = basename($file);
            $path = realpath($this->uploadDir . $file);
            if (!$path || $path == realpath($this->uploadDir)) {
                if ($file == 'default.png') {
                    if (__DIR__ . '/../www/imgs/default.png') {
                        copy(__DIR__ . '/../www/imgs/default.png', $this->uploadDir . $file);
                    }
                } else {
                    throw new \Exception('File not found');
                }
            }

            if ($lazy) {
                $dimensions = $this->getSize($size);

                if (count($dimensions) == 2) {
                    $size = 'lazy';
                }
            }

            $url = \dtw\DtW::$config->get('site.static') . sprintf('/uploads/%s/%s', $size, $file);

            return $url;
        }

        public function getDefault($size) {
            return $this->get('default.png', $size);
        }

        public function getSize($size) {
            if (!array_key_exists($size, $this->sizes)) {
                throw new \Exception('Invalid size');
            }

            return $this->sizes[$size];
        }

        public function upload($file) {
            if (!isset($file['error']) || is_array($file['error'])) {
                throw new \Exception('Invalid request');
            }

            switch ($file['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \Exception('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \Exception('Exceeded filesize limit.');
                default:
                    throw new \Exception('Unknown errors.');
            }

            if ($file['size'] > 10 * 1000 * 1000) {
                throw new \Exception('Exceeded filesize limit.');
            }

            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                $finfo->file($file['tmp_name']),
                array(
                    'jpg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif',
                ),
                true
            )) {
                throw new \Exception('Invalid file format.');
            }

            // Get image sizes
            if (!getimagesize($file['tmp_name'])) {
                throw new \Exception('Invalid file.');
            }

            $path = bin2hex(openssl_random_pseudo_bytes(16)) . '.' . $ext;
            $destination = $this->uploadDir . $path;

            if (!move_uploaded_file($file['tmp_name'], $destination)) {
                throw new \Exception('Failed to upload file');
            }

            return $path;
        }

        public function resize($file, $size) {
            if ($size && !array_key_exists($size, $this->sizes)) {
                throw new \Exception('Invalid image size');
            }

            // Does upload exist
            $file = basename($file);
            $path = realpath($this->uploadDir . $file);
            if (!$path || $path == $this->uploadDir) {
                throw new \Exception('Image not found');   
            }

            if ($size) {
                $dimensions = $this->sizes[$size];
            }

            if (!$dimensions || !is_array($dimensions)) {
                throw new \Exception('Invalid dimensions'); 
            }

            // Check for animated GIF
            $im = new \Imagick($this->uploadDir . $file);

            if ($im->getImageFormat() == 'GIF') {
                // Only retain animation for large formats
                if (in_array($size, array('bigavatar', 'full', 'medium', 'large'))) {
                    return $this->resizeAnimated($file, $im, $size);
                } else {
                    return $this->resizeStatic($file, $size);
                }
            } else {
                return $this->resizeStatic($file, $size);
            }
        }

        private function resizeStatic($file, $size) {
            $dimensions = $this->sizes[$size];

            // Load image
            $image = $this->manager->make($this->uploadDir . $file);
            
            if (count($dimensions) == 2) {
                // Resize
                $image->fit($dimensions[0], $dimensions[1], function($constraint) {
                    $constraint->upsize();
                });
            } else {
                // Resize
                $image->resize($dimensions[0], null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            // Save cache
            $path = $this->outputDir . $size;
            $outputPath = realpath($path);
            if (!$outputPath) {
                mkdir($path, 0777, true);
                $outputPath = realpath($path);
                if (!$outputPath) {
                    throw new \Exception('Unable to save cache');
                }
            }
            $outputPath .=  '/' . $file;
            $image->save($outputPath);
            
            echo $image->response('jpg', 70);
            die();
        }

        private function resizeAnimated($file, $im, $size) {
            $dimensions = $this->sizes[$size];

            $currentDimensions = $im->getImagePage();

            // Don't make pictures bigger than current size
            if ($currentDimensions['width'] < $dimensions[0]) {
                $dimensions[0] = $currentDimensions['width'];
            }

            // Save cache
            $path = $this->outputDir . $size;
            $outputPath = realpath($path);
            if (!$outputPath) {
                mkdir($path, 0777, true);
                $outputPath = realpath($path);
                if (!$outputPath) {
                    throw new \Exception('Unable to save cache');
                }
            }
            $outputPath .=  '/' . $file;

            $cmd = "gifsicle --resize-width {$dimensions[0]} --optimize=2 --no-warnings --conserve-memory ";
            $inputPath = $this->uploadDir . $file;

            shell_exec($cmd . $inputPath . ' > ' . $outputPath);

            header('Location: '.$_SERVER['PHP_SELF']);
        }
    }
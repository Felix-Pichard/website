<?php
    namespace dtw;

    class Playground {
        static $levels;

        public static function getLevels() {
            if (isset(self::$levels)) {
                return self::$levels;
            }

            $levels = \dtw\DtW::$redis->get('playground');
            if ($levels) {
                return json_decode($levels);
            } else {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `level_id` AS `ID`, `slug`
                    FROM `levels`
                    WHERE `status` = "live"
                    ORDER BY `levels`.`order` ASC, `levels`.`level_id` ASC
                '); 
                $stmt->execute(); 
                if ($stmt->rowCount()) {
                    $levels = $stmt->fetchAll();
                } else {
                    return array();
                }

                // Store in cache
                \dtw\DtW::$redis->set('playground', json_encode($levels));

                // Store for later use
                self::$levels = $levels;

                return $levels;
            }
        }

        public static function getLevelSubjects() {
            $subjects = \dtw\DtW::$redis->get('playground:subjects');

            if ($subjects) {
                return (array)json_decode($subjects);
            } else {
                $levels = self::getLevels();

                $subjects = array_map(function($l) {
                    $level = self::getByID($l->ID);
                    return (string)$level->subject;
                }, $levels);

                $subjects = array_count_values($subjects);

                unset($subjects['']);
                ksort($subjects);
                $intro = $subjects['Intro'];
                unset($subjects['Intro']);
                $subjects = array('Intro' => $intro) + $subjects;

                $data = array();
                foreach($subjects AS $subject => $count) {
                    $slug = strtolower($subject);
                    $data[$slug] = new \stdClass();
                    $data[$slug]->name = $subject;
                    $data[$slug]->count = $count;
                }

                // Store in cache
                \dtw\DtW::$redis->set('playground:subjects', json_encode($data));

                return $data;
            }
        }

        public static function getLevelsOrdered() {
            $levels = self::getLevels();

            // Sort levels
            usort($levels, function($a, $b) use ($app) {
                $aCompleted = \dtw\DtW::getInstance()->user->levels->{$a->ID}->hasCompleted;
                $bCompleted = \dtw\DtW::getInstance()->user->levels->{$b->ID}->hasCompleted;

                $diff = $aCompleted - $bCompleted;
                if ($diff !== 0) {
                    return $diff;
                } else {
                    $aLevel = \dtw\Playground::getById($a->ID);
                    $bLevel = \dtw\Playground::getById($b->ID);
                    $diff = $bLevel->meta->stats->completed - $aLevel->meta->stats->completed;
                    return $diff !== 0 ? $diff : 
                        ($bLevel->meta->stats->completedPercentage - $aLevel->meta->stats->completedPercentage);
                }
            });

            return $levels;
        }

        public static function getNextLevel() {
            $levels = self::getLevelsOrdered();
            $levelID = $levels[0]->ID;

            if (!\dtw\DtW::getInstance()->user->levels->{$levelID}->hasCompleted) {
                return $levelID;
            }
        }

        public static function getLevelCount() {
            return count(self::getLevels());
        }

        public static function getMaxPoints() {
            $points = 0;

            array_map(function($level) use (&$points) {
                $level = self::getByID($level->ID);
                switch ($level->difficulty) {
                    case 'bronze': $points += 5; break;
                    case 'silver': $points += 15; break;
                    case 'gold': $points += 25;
                }
            }, self::getLevels());

            return $points;
        }

        public static function getLevelsCompleted() {
            $key = 'playground:stats';
            $stats = \dtw\DtW::$redis->get($key);

            if ($stats) {
                $stats = json_decode($stats);
            } else {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT count(*) AS `completed`
                    FROM `user_levels`
                    WHERE `finished` IS NOT NULL
                '); 
                $stmt->execute();
                $row = $stmt->fetch();

                $stats = $row->completed;

                \dtw\DtW::$redis->set($key, json_encode($stats));
                \dtw\DtW::$redis->expire($key, 900);
            }

            return $stats;
        }

        public static function getByID($levelID) {
            $redisClassKey = 'level:class:' . $levelID;
            $levelClass = \dtw\DtW::$redis->get($redisClassKey);

            if (!$levelClass) {
                // Find level
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `levels`.`level_id`, `level_meta`.`value`
                    FROM `levels`
                    LEFT JOIN `level_meta`
                    ON `level_meta`.level_id = `levels`.level_id AND `level_meta`.key = "class"
                    WHERE `levels`.`level_id` = :level_id AND `status` = "live"
                    LIMIT 1
                '); 
                $stmt->execute(array(':level_id' => $levelID)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();

                    $levelClass = $row->value ? $row->value : 'BasicLevel';
                    \dtw\DtW::$redis->set($redisClassKey, $levelClass);
                } else {
                    throw new \Exception('Level not found');
                }
            }

            $class = "\dtw\\playground\\levels\\" . $levelClass;
            $level = new $class($levelID);

            return $level;
        }

        public static function getBySlug($slug) {
            $redisKey = 'level:slug:' . $slug;
            // Lookup slug in Redis
            $ID = \dtw\DtW::$redis->get($redisKey);

            // If the link wasn't found, check DB
            if (!$ID) {

                $stmt = \dtw\DtW::$db->prepare('SELECT level_id FROM levels WHERE `slug` = :slug'); 
                $stmt->execute(array(':slug' => $slug)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    $ID = $row->level_id;
                } else {
                    throw new \Exception('Level not found');
                }
            }

            try {
                // Cache
                \dtw\DtW::$redis->set($redisKey, $ID);

                return self::getByID($ID);
            } catch (\Exception $e) {
                throw $e;
            }
        }


        public static function createLevel($data) {
            if (!\dtw\DtW::getInstance()->user->hasPrivilege('playground.edit')) {
                throw new \Exception('You do not have privileges to complete this action');
            }

            $defaults = array(
                'status' => 'live',
                'difficulty' => 'bronze'
            );

            $requiredFields = array(
                'title',
                'slug'
            );
            
            // Check all required fields are present
            foreach ($requiredFields as $field) {
                if (!array_key_exists($field, $data) || !isset($data[$field]) || !$data[$field]) {
                    throw new \Exception('Missing fields!!');
                }
            }

            // Merge defaults and data
            $data = array_merge($defaults, $data);


            // Create level
            $stmt = \dtw\DtW::$db->prepare('
                INSERT INTO levels (`title`, `slug`, `difficulty`, `status`, `subject`)
                VALUES (:title, :slug, :difficulty, :status, :subject);
            ');
            $stmt->execute(array(
                ':title' => $data['title'],
                ':slug' => $data['slug'],
                ':difficulty' => $data['difficulty'],
                ':status' => $data['status'],
                ':subject' => $data['subject']
            ));

            $id = \dtw\DtW::$db->lastInsertId();


            // Create level meta
            $meta = array('description', 'class', 'form', 'answer');
            
            // Tidy answer if present
            if (isset($data['answer']) && count($data['answer'])) {
                $answer = array();
                foreach($data['answer'] AS $tmpAnswer) {
                    if ($tmpAnswer['key'] && $tmpAnswer['value']) {
                        $answer[$tmpAnswer['key']] = $tmpAnswer['value'];
                    }
                }

                if (count($answer)) {
                    $data['answer'] = json_encode($answer);
                }
            } else {
                unset($data['answer']);
            }

            foreach ($meta as $key) {
                if ($data[$key]) {
                    $stmt = \dtw\DtW::$db->prepare('
                        INSERT INTO level_meta (`level_id`, `key`, `value`)
                        VALUES (:id, :key, :value);
                    ');
                    $stmt->execute(array(
                        ':id' => $id,
                        ':key' => $key,
                        ':value' => $data[$key],
                    ));
                }
            }

            
            // Clear cache    

            //TODO make this less aggressive
            \dtw\DtW::$redis->flushall();


            return $data['slug'];

        }

        public static function getLevelEditorForm($level = null) {
            if ($level) {
                $form = new \dtw\utils\Form($level->title, "Update", array('block' => true));
            } else {
                $form = new \dtw\utils\Form("New level", "Create", array('block' => true));
            }

            $answers = array();

            if (isset($level->answer) && !is_array($level->answer) && count((array) $level->answer)) {
                foreach((array) $level->answer AS $key => $value) {
                    $answers[] = array(
                        'key' => $key,
                        'value' => $value
                    );
                }
            }

            $form->addField('Title', "text", array('value' => isset($level->title) ? $level->title : '' ));
            $form->addField('Slug', "text", array( 'columns' => 'four', 'groupStart' => true, 'value' => isset($level->slug) ? $level->slug : '' ));
            $form->addField('Status', "select", array( 'columns' => 'four', 'options' => array( 'draft' => 'Draft', 'live' => 'Live' ), 'value' => isset($level->status) ? $level->status : '' ));
            $form->addField('Subject', "text", array( 'columns' => 'four', 'groupEnd' => true, 'value' => isset($level->subject) ? $level->subject : '' ));
            $form->addField('Description', "markdown", array('value' => isset($level->meta->description) ? $level->meta->description : '' ));
            $form->addField('Solution', "markdown", array('value' => isset($level->meta->solution) ? $level->meta->solution : '' ));
            $form->addField('Form', "select", array( 'columns' => 'three', 'groupStart' => true, 'options' => array( '' => 'Custom', 'userpass' => 'Username and password', 'pass' => 'Password only' ), 'value' => isset($level->meta->form) ? $level->meta->form : '' ));
            $form->addField('Class', "text", array( 'columns' => 'three', 'value' => isset($level->meta->class) ? $level->meta->class : '' ));
            $form->addField('Difficulty', "select", array( 'columns' => 'three', 'options' => array( 'bronze' => 'Bronze', 'silver' => 'Silver', 'gold' => 'Gold' ), 'value' => isset($level->difficulty) ? $level->difficulty : '' ));
            $form->addField('Timer', "text", array( 'columns' => 'three', 'groupEnd' => true, 'value' => isset($level->timer) ? $level->timer : '' ));
            $form->addField('Answer', "repeater",
                array(
                    'fields' => array(
                        'key' => 'Key',
                        'value' => 'Value'
                    ),
                    'values' => $answers
                )
            );
            $form->addField('Extras', "markdown", array('value' => isset($level->meta->extras) ? $level->meta->extras : '' ));
            $form->addField('Online', "text", array('value' => isset($level->meta->online) ? $level->meta->online : '' ));

            return $form;
        }

        public static function registerHooks() {
            // Register API hooks
            if (\dtw\DtW::$api) {
                \dtw\DtW::$api->registerHook('playground', 'lookup', function($params) {
                    $q = trim($_GET['q']);
                    $q = preg_replace("/[^0-9A-Za-z_.-]/", "", $q);

                    if (!strlen($q)) {
                        throw new \Exception('No search term');
                    }

                    $levels = \dtw\Playground::searchLevels(array(
                        'q' => '%' . $q . '%',
                        'limit' => 5
                    ));

                    if (!$levels) {
                        throw new \Exception('No results');
                    }

                    return array(
                        'results' => array_map(function($item) {
                            $result = new \stdClass();
                            $result->text = $item->title;
                            $result->replace = $item->id;
                            return $result;
                        }, $levels)
                    );
                });
            }
        }

    }
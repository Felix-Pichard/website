<?php
    namespace dtw\user;
    use Macaroons\Macaroon;

    class User {
        private $_user;

        public function __construct() {
            if (isset($_SESSION['user'])) {
                $this->_user = $_SESSION['user'];

                $this->settings = new Settings($this->_user->id);
                $this->levels = new Levels($this->_user->id);
                $this->profile = new Profile($this->_user->id, true);
                $this->notifications = new Notifications($this->_user->id);

                $this->_setActivity();

                // Check for privileges change
                $key = 'user:' . $this->_user->id . ':privileges';
                $privileges = \dtw\DtW::$redis->get($key);
                if ($privileges) {
                    $this->_user->privileges = ($privileges == 'unset' ? null : $privileges);
                    $_SESSION['user'] = $this->_user;
                    \dtw\DtW::$redis->del($key);
                }
            }
        }

        public function getNextMedal($force = false) {
            if (!$force && $this->_user && property_exists($this->_user, 'nextMedal')) {
                $medal = \dtw\Medals::getMedal($this->_user->nextMedal->id);

                if (!$medal->hasCompleted()) {
                    return $this->_user->nextMedal;
                }
            }

            $medal = \dtw\Medals::getUserMedal();
            $this->_user->nextMedal = new \stdClass();
            $this->_user->nextMedal->label = $medal->label;
            $this->_user->nextMedal->id = $medal->id;
            $this->_user->nextMedal->colour = $medal->colour;
            $this->_user->nextMedal->progress = $medal->progress;

            $_SESSION['user'] = $this->_user;

            return $this->_user->nextMedal;
        }

        public function setNextMedal($progress) {
            if (!$this->_user->nextMedal->id) {
                return;
            }

            if ($progress === null) {
                unset($this->_user->nextMedal);
            } else {
                $this->_user->nextMedal->progress = $progress;
            }

            $_SESSION['user'] = $this->_user;
        }

        public function __get($name) {
            if ($this->_user && property_exists($this->_user, $name)) {
                return $this->_user->$name;
            }
            return null;
        }

        public function __isset($name) {
            if (empty($this->_user)) return false;

            return property_exists($this->_user, $name);
        }


        public function isAuth($response = null) {
            // Redirect user back if they don't have access
            if ($response && empty($this->_user)) {
                // Store where the user came from to send them back there
                if (!empty($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '/') {
                    $_SESSION['auth.redirect'] = $_SERVER['REQUEST_URI'];
                }

                $response->redirect('/auth', 403)->send();
                die();
            }

            return !empty($this->_user);
        }

        public function login($username, $password, $remember) {
            if (!$username || empty($username) ||
                !$password || empty($password)) {
                    throw new \Exception('Missing field');
            }

            // Lookup user
            $stmt = \dtw\DtW::$db->prepare('
                SELECT user_id, username, password, email, privileges, twoFactor
                FROM users
                WHERE `username` = :username AND `password` IS NOT NULL
            '); 
            $stmt->execute(array(':username' => $username)); 
            if ($stmt->rowCount()) {
                $row = $stmt->fetch();

                // Is the password correct
                if (!$this->_checkHash($row->password, $password)) {
                    throw new \Exception('Invalid login details');
                }

                // Check or create browser identification
                if (isset($_COOKIE['auth_remember'])) {
                    $token = $_COOKIE['auth_remember'];
                    // Is the token associated with this account?
                    $stmt = \dtw\DtW::$db->prepare('
                        SELECT user_id
                        FROM user_browsers
                        WHERE `token` = :token
                    '); 
                    $stmt->execute(array(
                        ':token' => $token
                    )); 
                    if ($stmt->rowCount()) {
                        $duplicate = $stmt->fetch();

                        if ($duplicate->user_id != $row->user_id) {
                            // Forget token
                            unset($_COOKIE['auth_remember']);

                            // Remove old session as it wont be used again
                            $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_browsers WHERE `user_id` = :id AND `token` = :token');
                            $stmt->execute(array(
                                ':id' => $duplicate->user_id,
                                ':token' => $token
                            ));
                        }
                    } else {
                        unset($_COOKIE['auth_remember']);
                    }
                }

                if (!$_COOKIE['auth_remember']) {
                    $token = \dtw\utils\Utils::generateToken();

                    // Store cookie
                    $stmt = \dtw\DtW::$db->prepare('INSERT INTO user_browsers (`user_id`, `token`, `login`) VALUES (:id, :token, :remember)');
                    $stmt->execute(array(
                        ':id' => $row->user_id,
                        ':token' => $token,
                        ':remember' => $remember ? 1 : 0
                    ));

                    setcookie('auth_remember', $token, time() + (10 * 365 * 24 * 60 * 60));
                } else if ($remember) {
                    $token = $_COOKIE['auth_remember'];

                    // Update token to remember me
                    $stmt = \dtw\DtW::$db->prepare('UPDATE `user_browsers` SET `login` = 1 WHERE `user_id` = :id AND `token` = :token'); 
                    $stmt->execute(array(
                        ':id' => $row->user_id,
                        ':token' => $token
                    ));
                }

                // Is 2FA enabled
                if ($row->twoFactor) {
                    // Do they have a remember me
                    $stmt = \dtw\DtW::$db->prepare('
                        SELECT user_id
                        FROM user_browsers
                        WHERE `token` = :token AND `user_id` = :id AND `twoFactor` = 1
                    '); 
                    $stmt->execute(array(
                        ':id' => $row->user_id,
                        ':token' => $token
                    ));

                    if (!$stmt->rowCount()) {
                        // Redirect to 2FA page
                        $user = new \stdClass();
                        $user->id = $row->user_id;
                        $user->secret = $row->twoFactor;
                        $user->remember = $remember;
                        $_SESSION['twoFactorUser'] = $user;
                        return false;
                    }
                }

                return $this->_createSession($row);
            } else {
                throw new \Exception('Invalid login details');
            }
        }

        public function loginWithToken($userID, $token) {
            $valid = \dtw\Auth::checkToken($userID, $token);

            if (!$valid) {
                throw new \Exception('Token is invalid or has expired');
            }

            // Lookup user
            $stmt = \dtw\DtW::$db->prepare('
                SELECT user_id, username, password, email, privileges, twoFactor
                FROM users
                WHERE `user_id` = :userID
            '); 
            $stmt->execute(array(':userID' => $userID)); 
            if ($stmt->rowCount()) {
                $row = $stmt->fetch();

                $_SESSION['allowPasswordReset'] = true;

                // Is 2FA enabled
                if (isset($_COOKIE['auth_remember'])) {
                    $remember = $_COOKIE['auth_remember'];

                    if ($row->twoFactor) {
                        // Do they have a remember me
                        $stmt = \dtw\DtW::$db->prepare('
                            SELECT user_id
                            FROM user_browsers
                            WHERE `token` = :token AND `user_id` = :id AND `twoFactor` = 1
                        '); 
                        $stmt->execute(array(
                            ':id' => $row->user_id,
                            ':token' => $remember
                        ));

                        if (!$stmt->rowCount()) {
                            // Redirect to 2FA page
                            $user = new \stdClass();
                            $user->id = $row->user_id;
                            $user->secret = $row->twoFactor;
                            $_SESSION['twoFactorUser'] = $user;
                            return false;
                        }
                    }
                }

                return $this->_createSession($row);
            } else {
                throw new \Exception('Token is invalid or has expired 2');
            }
        }

        public function checkRememberMe() {
            if (!isset($_COOKIE['auth_remember'])) return false;

            $token = $_COOKIE['auth_remember'];
            $stmt = \dtw\DtW::$db->prepare('
                SELECT users.user_id, username, password, email, privileges, users.twoFactor, user_browsers.twoFactor AS `twoFactorRemember`
                FROM user_browsers
                INNER JOIN users
                ON user_browsers.user_id = users.user_id
                WHERE `user_browsers`.`token` = :token AND `login` = 1'); 
            $stmt->execute(array(':token' => $token)); 
            if ($stmt->rowCount()) {
                $row = $stmt->fetch();

                // Do they still need to verify 2FA
                if ($row->twoFactor && !$row->twoFactorRemember) {
                    return false;
                } else {
                    return $this->_createSession($row);
                }
            }

            return false;
        }

        public function disableRememberMe() {
            if (!isset($_COOKIE['auth_remember'])) return;

            $token = $_COOKIE['auth_remember'];
            $stmt = \dtw\DtW::$db->prepare('UPDATE `user_browsers` SET `login` = 0 WHERE `user_id` = :user_id AND `token` = :token'); 
            $stmt->execute(array(':user_id' => $this->id, ':token' => $token));
        }

        public function logout() {
            $this->disableRememberMe();

            session_destroy();            
        }

        private function _createSession($data) {
            $session = new \stdClass();
            $session->id = $data->user_id;
            $session->username = $data->username;
            $session->email = $data->email;
            $session->privileges = $data->privileges;

            if (!empty($data->twoFactor)) {
                $session->twoFactor = $data->twoFactor;
            }

            $_SESSION['user'] = $session;

            // Load in users notifcations
            $this->notifications = new Notifications($session->id);
            $this->notifications->load();

            // Clean up session
            unset($_SESSION['newUser']);
            unset($_SESSION['twoFactorUser']);
            unset($_SESSION['lusitanian-oauth-state']);
            unset($_SESSION['lusitanian-oauth-token']);            

            // Check veteran status
            \dtw\Medals::checkUsersMedals('member', $this->id);

            return true;
        }

        public function hasPrivilege($privilege) {
            if ($privilege == 'guest') {
                return !$this->isAuth();
            }

            if ($privilege == 'contribute') {
                return $this->_user->privileges !== 'black';
            }

            if ($this->isAuth() && isset($this->_user->privileges)) {
                if ($this->_user->privileges !== 'black') {
                    $levels = \dtw\DtW::$config->get('privileges');

                    if (array_key_exists($privilege, $levels)) {
                        $requiredLevel = $levels[$privilege];
                    } else {
                        $requiredLevel = $privilege;
                    }

                    switch ($requiredLevel) {
                        case 'green':
                            return ($this->_user->privileges == 'green');
                        case 'gold':
                            return ($this->_user->privileges == 'green' || $this->_user->privileges == 'gold');
                        case 'silver':
                            return ($this->_user->privileges != 'bronze' );
                        case 'bronze':
                            return true;
                        default:
                            echo 'abcd' . $requiredLevel;
                            die();
                    }
                }
            }

            return false;
        }

        public function canContribute($response = null, $service = null) {
            $allowed = $this->hasPrivilege('contribute');

            // Redirect user back if they don't have access
            if ($response && !$allowed) {
                \dtw\utils\Flash::add('You have been banned from contributing to this site', 'error');
                $response->redirect('/dashboard')->send();
                die();
            }

            return $allowed;
        }

        public function oauth($service, $token) {
            if ($this->isAuth()) {
                try {
                    $this->linkOauth($service, $token);
                    \dtw\utils\Flash::add("{$provider} has been enabled" , 'success');
                } catch (\Exception $e) {
                    \dtw\utils\Flash::add("This {$provider} account has already been linked to another DtW account" , 'error');
                }
                header('Location: /settings/oauth');
                die();
            } else {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT users.user_id, users.username, users.email, users.privileges
                    FROM user_auth
                    INNER JOIN users
                    ON users.user_id = user_auth.user_id
                    WHERE `service` = :service AND `token` = :token'); 
                $stmt->execute(array(':service' => $service, 'token' => $token)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    return $this->_createSession($row);
                }

                return false;
            }
        }

        public function getConnectedOauth() {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `service`
                    FROM user_auth
                    WHERE `user_id` = :id
                '); 
                $stmt->execute(array(
                    ':id' => $this->id
                )); 
                return $stmt->fetchAll(\PDO::FETCH_COLUMN);
        }

        public function linkOauth($service, $token) {
            $stmt = \dtw\DtW::$db->prepare('INSERT INTO user_auth (`user_id`, `service`, `token`)
                                            VALUES (:id, :service, :token)'); 
            $stmt->execute(array(
                ':id' => $this->id,
                ':service' => $service,
                ':token' => $token
            ));
        }

        public function unlinkOauth($service) {
            $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_auth WHERE `user_id` = :id AND `service` = :service'); 
            $stmt->execute(array(
                ':id' => $this->id,
                ':service' => $service
            ));

            \dtw\utils\Flash::add("{$provider} has been disabled" , 'success');
        }

        public function create($details) {
            if (!array_key_exists("username", $details) ||
                empty($details['username']) ||
                !array_key_exists("email", $details) ||
                empty($details['email'])) {
                    throw new \Exception('Missing field');
            }

            // Is this an oauth account?
            $hash = null;
            $verified = 0;
            if (array_key_exists("service", $details)) {
                if (!array_key_exists("token", $details)) {
                    throw new \Exception('Missing service token');
                }
                $verified = 1;
            } else {
                if (!array_key_exists("password", $details)) {
                    throw new \Exception("Missing field");
                }
                $hash = $this->_generateHash($details['password']);
            }

            // Check email is valid
            $this->_checkForEmail($details['email']); // throws error

            // Check username is valid
            $this->_checkForUsername($details['username']); // throws error

            // $avatar = null;
            // if (!empty($details['avatar'])) {
            //     $avatar = $details['avatar'];
            // }

            \dtw\DtW::$db->beginTransaction();

            try {
                // Create user
                $stmt = \dtw\DtW::$db->prepare('INSERT INTO users (`username`, `password`, `email`, `verified`)
                                                VALUES (:username, :password, :email, :verified)'); 
                $stmt->execute(array(
                    ':username' => $details['username'],
                    ':password' => $hash,
                    ':email' => $details['email'],
                    ':verified' => $verified
                ));

                // Get user id
                $details['id'] = \dtw\DtW::$db->lastInsertId();

                // Add oauth
                if (array_key_exists("service", $details)) {
                    $stmt = \dtw\DtW::$db->prepare('INSERT INTO user_auth (`user_id`, `service`, `token`)
                                                    VALUES (:id, :service, :token)'); 
                    $stmt->execute(array(
                        ':id' => $details['id'],
                        ':service' => $details['service'],
                        ':token' => $details['token'],
                    ));
                }

                \dtw\DtW::$db->commit();
            } catch (\Exception $e) {
                // roll back transaction
                \dtw\DtW::$db->rollback();
                throw new \Exception('Error creating user');
            }

            
            $info = new \stdClass();
            $info->user_id = $details['id'];
            $info->username = $details['username'];
            $info->email = $details['email'];

            // Send welcome email
            \dtw\utils\Emailer::send($info->user_id, 'Welcome to Defend the Web', 'welcome', $info);

            // Add to search
            $profile = new Profile($info->user_id, true);
            $profile->buildSearch();

            // Add to feed
            \dtw\utils\Feed::addItemToFeed(array(
                'type' => 'user.join',
                'user' => $info->user_id,
                'username' => $info->username
            ));

            // Login
            $this->_createSession($info);
        }

        public function enable2FA($secret) {
            try {
                $stmt = \dtw\DtW::$db->prepare('UPDATE `users` SET `twoFactor` = :secret WHERE `user_id` = :user_id'); 
                $stmt->execute(array(':user_id' => $this->id, ':secret' => $secret));

                $this->_user->twoFactor = $secret;
                $_SESSION['user'] = $this->_user;
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }

        public function disable2FA() {
            try {
                $stmt = \dtw\DtW::$db->prepare('UPDATE `users` SET `twoFactor` = null WHERE `user_id` = :user_id'); 
                $stmt->execute(array(':user_id' => $this->id));

                $this->_user->twoFactor = null;
                $_SESSION['user'] = $this->_user;
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }

        public function verify2FA($code, $remember) {
            $session = $_SESSION['twoFactorUser'];

            if (!$session || !$code) {
                throw new \Exception('Missing code');
            }

            $twoFactor = new \dtw\utils\TwoFactor();
            $checkResult = $twoFactor->verifyCode($session->secret, $code, 1);
            if ($checkResult) {
                // Lookup user
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT user_id, username, email, privileges, twoFactor
                    FROM users
                    WHERE `user_id` = :id'); 
                $stmt->execute(array(':id' => $session->id)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();

                    if (($remember || $session->remember) && isset($_COOKIE['auth_remember'])) {
                        $token = $_COOKIE['auth_remember'];
                        $stmt = \dtw\DtW::$db->prepare('UPDATE `user_browsers` SET `twoFactor` = 1 WHERE `user_id` = :id AND `token` = :token'); 
                        $stmt->execute(array(
                            ':id' => $row->user_id,
                            ':token' => $token
                        ));
                    }

                    return $this->_createSession($row);
                } else {
                    throw new \Exception('Invalid account');
                }

                die();
            } else {
                throw new \Exception('Invalid code');
            }
        }

        public function update($updates) {
            $DtW = \dtw\DtW::getInstance();

            try {
                \dtw\DtW::$db->beginTransaction();

                $allowed = array('status', 'bio', 'signature', 'name', 'website', 'feedVisible', 'medalsVisible', 'followersVisible');

                foreach($updates AS $key => $value) {
                    if (!in_array($key, $allowed)) {
                        continue; // User is not allowed to update this field
                    }

                    // Update database
                    $stmt = \dtw\DtW::$db->prepare('INSERT INTO `user_profile_meta` (`user_id`, `key`, `value`) VALUES (:id, :key, :value) ON DUPLICATE KEY UPDATE `value` = :value'); 
                    $stmt->execute(array(
                        ':id' => $this->id,
                        ':key' => $key,
                        ':value' => $value
                    ));
                }

                // Upload image
                if (file_exists($_FILES['avatar']['tmp_name']) && is_uploaded_file($_FILES['avatar']['tmp_name'])) {
                    $DtW->load('Images');

                    $avatar = $DtW->images->upload($_FILES['avatar']);

                    // Update database
                    $stmt = \dtw\DtW::$db->prepare('INSERT INTO `user_profile_meta` (`user_id`, `key`, `value`) VALUES (:id, :key, :value) ON DUPLICATE KEY UPDATE `value` = :value'); 
                    $stmt->execute(array(
                        ':id' => $this->id,
                        ':key' => 'avatar',
                        ':value' => $avatar
                    ));

                    // Award medal
                    \dtw\Medals::awardMedal('Cheese');
                } else if (isset($updates['removeImage'])) {
                    $stmt = \dtw\DtW::$db->prepare('DELETE FROM `user_profile_meta` WHERE `user_id` = :id AND `key` = "avatar"'); 
                    $stmt->execute(array(
                        ':id' => $this->id
                    ));
                } else if ($_FILES['avatar'] && $_FILES['avatar']['error'] !== UPLOAD_ERR_NO_FILE) {
                    switch ($_FILES['avatar']['error']) {
                        case UPLOAD_ERR_INI_SIZE:
                            throw new \Exception("The uploaded file exceeds the upload_max_filesize directive in php.ini");
                        case UPLOAD_ERR_FORM_SIZE:
                            throw new \Exception("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form");
                        case UPLOAD_ERR_PARTIAL:
                            throw new \Exception("The uploaded file was only partially uploaded");
                        case UPLOAD_ERR_NO_TMP_DIR:
                            throw new \Exception("Missing a temporary folder");
                        case UPLOAD_ERR_CANT_WRITE:
                            throw new \Exception("Failed to write file to disk");
                        case UPLOAD_ERR_EXTENSION:
                            throw new \Exception("File upload stopped by extension");
                        default:
                            throw new \Exception("Unknown upload error");
                    } 
                }

                \dtw\DtW::$db->commit();

                // Clear profile
                \dtw\Users::purgeUser($this->id);

                // Update session
                $_SESSION['user'] = $this->_user;
            } catch (Exception $e) {
                \dtw\DtW::$db->rollback();
                throw $e;
            }

            return true;
        }

        public function changeUsername($username) {
            if (strtolower($username) === strtolower($this->_user->username)) {
                throw new \Exception('That is already your username');
            }

            $this->_checkForUsername($username);

            try {
                \dtw\DtW::$db->beginTransaction();

                // Update username
                $stmt = \dtw\DtW::$db->prepare('UPDATE users SET `username` = :username WHERE `user_id` = :id');
                $stmt->execute(array(
                    ':id' => $this->id,
                    ':username' => $username
                ));

                // Add usename to list of reserved
                $stmt = \dtw\DtW::$db->prepare('INSERT INTO `user_usernames` (`user_id`, `username`) VALUES (:id, :username)'); 
                $stmt->execute(array(
                    ':id' => $this->id,
                    ':username' => $this->_user->username
                ));

                \dtw\DtW::$db->commit();

                // Clear profile
                \dtw\Users::purgeUser($this->id);

                // Delete username lookup
                \dtw\DtW::$redis->get('username:' . $this->_user->username);

                // Update search
                $profile = new Profile($this->id, true);
                $profile->buildSearch();

                $this->_user->username = $username;

                // Update session
                $_SESSION['user'] = $this->_user;
            } catch (Exception $e) {
                \dtw\DtW::$db->rollback();
                throw $e;
            }
        }

        public function changePassword($currentPassword, $newPassword, $newPasswordCheck) {
            // Check all fields
            if (!$newPassword || !strlen($newPassword) ||
                !$newPasswordCheck || !strlen($newPasswordCheck)) {
                    throw new \Exception('All fields are required');
            }

            // Check current password
            if (!isset($_SESSION['allowPasswordReset']) && !$_SESSION['allowPasswordReset']) {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT password
                    FROM users
                    WHERE `user_id` = :userID AND `password` IS NOT NULL'); 
                $stmt->execute(array(':userID' => $this->_user->id)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();

                    if (!$this->_checkHash($row->password, $currentPassword)) {
                        throw new \Exception('Incorrect password');
                    }
                } else {
                    throw new \Exception('Something weird happened');
                }
            }

            // Check new passwords match
            if ($newPassword !== $newPasswordCheck) {
                throw new \Exception('New passwords don\'t match');
            }

            $hash = $this->_generateHash($newPassword);
            $stmt = \dtw\DtW::$db->prepare('UPDATE users SET `password` = :password WHERE `user_id` = :id');
            $stmt->execute(array(
                ':id' => $this->id,
                ':password' => $hash
            ));

            unset($_SESSION['allowPasswordReset']);
        }

        public function changeEmail($currentPassword, $newEmail, $newEmailCheck) {
            // Check all fields
            if (!$newEmail || !strlen($newEmail) ||
                !$newEmailCheck || !strlen($newEmailCheck)) {
                    throw new \Exception('All fields are required');
            }

            // Check current email
            $stmt = \dtw\DtW::$db->prepare('
                SELECT password
                FROM users
                WHERE `user_id` = :userID AND `password` IS NOT NULL'); 
            $stmt->execute(array(':userID' => $this->_user->id)); 
            if ($stmt->rowCount()) {
                $row = $stmt->fetch();

                if (!$this->_checkHash($row->password, $currentPassword)) {
                    throw new \Exception('Incorrect password');
                }
            } else {
                throw new \Exception('Something weird happened');
            }

            // Check new emails match
            if ($newEmail !== $newEmailCheck) {
                throw new \Exception('New emails don\'t match');
            }

            $this->_checkForEmail($newEmail); // Throws error

            $stmt = \dtw\DtW::$db->prepare('UPDATE users SET `email` = :email WHERE `user_id` = :id');
            $stmt->execute(array(
                ':id' => $this->id,
                ':email' => $newEmail
            ));
            $this->_user->email = $newEmail;
        }

        public function deleteAccount($password) {
            // Check current password
            $stmt = \dtw\DtW::$db->prepare('
                SELECT password
                FROM users
                WHERE `user_id` = :userID AND `password` IS NOT NULL'); 
            $stmt->execute(array(':userID' => $this->_user->id)); 
            if ($stmt->rowCount()) {
                $row = $stmt->fetch();

                if (!$this->_checkHash($row->password, $password)) {
                    throw new \Exception('Incorrect password');
                }
            } else {
                throw new \Exception('Something weird happened');
            }

            try {
                \dtw\DtW::$db->beginTransaction();

                // Update content
                $stmt = \dtw\DtW::$db->prepare('UPDATE `articles` SET `author` = NULL WHERE `author` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('UPDATE `forum_thread_posts` SET `author` = NULL WHERE `author` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('UPDATE `forum_threads` SET `author` = NULL WHERE `author` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('UPDATE `user_usernames` SET `user_id` = NULL WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('UPDATE `pm_messages` SET `user_id` = NULL WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));


                // Delete content
                $stmt = \dtw\DtW::$db->prepare('DELETE FROM forum_flags WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM pm_users WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_activity WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_browsers WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_auth WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_following WHERE `user_id` = :user_id OR `following` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_medals WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_levels WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_meta WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_notifications WHERE `to` = :user_id OR `from` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_profile_meta WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM user_settings WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                $stmt = \dtw\DtW::$db->prepare('DELETE FROM users WHERE `user_id` = :user_id');
                $stmt->execute(array(':user_id' => $this->_user->id));

                // Add username to reserverd usernames
                $stmt = \dtw\DtW::$db->prepare('
                    INSERT INTO user_usernames (`user_id`, `username`)
                    VALUES (null, :username)
                ');
                 $stmt->execute(array(
                    ':username' => $this->_user->username
                ));

                \dtw\DtW::$db->commit();

                // Remove from search
                \dtw\DtW::getInstance()->load('Search');
                try {
                    \dtw\DtW::getInstance()->search->client->delete([
                        'index' => 'users',
                        'id' => $this->_user->id
                    ]);
                } catch (\Exception $e) {
                    //
                }

                // Logout user
                $this->logout();
            } catch (\Exception $e) {
                \dtw\DtW::$db->rollback();

                var_dump($e);
                die();

                throw $e;
            }
        }

        private function _setActivity() {
            // Only update every 2 mins
            if (isset($this->_user->lastSetActivity) AND $this->_user->lastSetActivity > time() - 120) {
                return;
            }

            // Was the last time they were active not today?
            if (!isset($this->_user->lastSetActivity) || date('Ymd', $this->_user->lastSetActivity) !== date('Ymd')) {
                // Get last active from DB and see if this is a new consecutive day
                $stmt = \dtw\DtW::$db->prepare('SELECT active, consecutive, consecutive_max, days FROM user_activity WHERE `user_id` = :user_id');
                $stmt->execute(array(
                    ':user_id' => $this->id
                ));
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    $active = strtotime($row->active);

                    $this->_user->activity = $row;

                    $isToday = date('Ymd', $active) == date('Ymd');
                    $isYesterday = date('Ymd', $active) == date('Ymd', strtotime('yesterday'));

                    if ($isToday) {
                        // Do nothing
                    } else if ($isYesterday) {
                        $row->consecutive++;
                        $row->days++;

                        // Increase consecutive
                        $stmt = \dtw\DtW::$db->prepare('UPDATE `user_activity` SET `consecutive` = :count, `days` = :days WHERE `user_id` = :user_id'); 
                        $stmt->execute(array(':user_id' => $this->id, ':count' => $row->consecutive, ':days' => $row->days));

                        if ($row->consecutive > $row->consecutive_max) {
                            // Set max and check medals
                            $stmt = \dtw\DtW::$db->prepare('UPDATE `user_activity` SET `consecutive_max` = :count WHERE `user_id` = :user_id'); 
                            $stmt->execute(array(':user_id' => $this->id, ':count' => $row->consecutive));

                            \dtw\Medals::checkUsersMedals('logins', $this->id);
                        }

                        // Check veteran status
                        \dtw\Medals::checkUsersMedals('member', $this->id);

                        // Check days status
                        \dtw\Medals::checkUsersMedals('days', $this->id);
                    } else {
                        $row->days++;

                        // Reset consecutive count and update day count
                        $stmt = \dtw\DtW::$db->prepare('UPDATE `user_activity` SET `consecutive` = 1, `days` = :days WHERE `user_id` = :user_id'); 
                        $stmt->execute(array(':user_id' => $this->id, ':days' => $row->days));

                        // Check veteran status
                        \dtw\Medals::checkUsersMedals('member', $this->id);

                        // Check days status
                        \dtw\Medals::checkUsersMedals('days', $this->id);
                    }
                }
            }

            $this->_user->lastSetActivity = time();
            $_SESSION['user'] = $this->_user;

            $stmt = \dtw\DtW::$db->prepare('
                INSERT INTO user_activity (`user_id`, `active`)
                VALUES (:user_id, NOW())
                ON DUPLICATE KEY UPDATE `active` = NOW()
            ');
             $stmt->execute(array(
                ':user_id' => $this->id
            ));
        }

        public function getUnreadPM() {
            return \dtw\messages\Messages::getUnread();
        }

        private function _generateHash($password) {
            return password_hash($password, PASSWORD_DEFAULT, [ 'cost' => 14 ]);
        }

        private function _checkHash($hash, $password) {
            $passwordInverted = strtolower($password) ^ strtoupper($password) ^ $password; // Flip case

            return (password_verify($password, $hash) || // Check password as entered
                password_verify(substr($password, 0, count($password) - 1), $hash) || // Check password without last character
                password_verify(lcfirst($password), $hash) || // Check password with lowercased first letter
                password_verify($passwordInverted, $hash)); // Check password with case inverted
        }

        private function _checkForEmail($email) {
            // Is it a valid email?
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception('Invalid email');
            }

            // Has the email been used before
            $stmt = \dtw\DtW::$db->prepare('SELECT user_id FROM users WHERE `email` = :email'); 
            $stmt->execute(array(':email' => $email)); 
            if ($stmt->rowCount()) {
                throw new \Exception('Email is already registered');
            }
        }

        private function _checkForUsername($username) {
            // Is it a valid username
            if (strlen($username) < 3) {
                throw new \Exception('Username must be 3 or more characters');
            }
            if (preg_match('/[^0-9A-Za-z_.-]/', $username)) {
                throw new \Exception('Invalid username');
            }
            if ($username[0] == ".") {
                throw new \Exception('Username can not start or end with a .');
            }
            if ($username[strlen($username)-1] == ".") {
                throw new \Exception('Username can not start or end with a .');
            }

            // Has the username been used before
            $stmt = \dtw\DtW::$db->prepare('SELECT user_id FROM users WHERE `username` = :username'); 
            $stmt->execute(array(':username' => $username)); 
            if ($stmt->rowCount()) {
                throw new \Exception('Username is not available');
            }

            // Is it reserved
            $stmt = \dtw\DtW::$db->prepare('SELECT user_id FROM user_usernames WHERE `username` = :username AND `user_id` != :id AND `changed` > (NOW() - INTERVAL 1 MONTH)'); 
            $stmt->execute(array(
                ':id' => $this->id,
                ':username' => $username
            )); 
            if ($stmt->rowCount()) {
                throw new \Exception('Username is not available');
            }
        }

        public function getMatrixToken($type = "jwt") {
            if (!$this->isAuth()) return;

            if ($this->_user->matrix && $type == "jwt") {
                return $this->_user->matrix;
            }

            if ($type == "jwt") {
                $key = \dtw\DtW::$config->get('matrix.jwt');
                $details = array(
                    "sub" => $this->_user->username,
                    "iat" => time(),
                    "nbf" => time(),
                    "exp" => time() + 60
                );

                $token = \Firebase\JWT\JWT::encode($details, $key);

                $payload = json_encode(array(
                    "type" => "m.login.jwt",
                    "token" => $token
                ));

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://' . \dtw\DtW::$config->get('matrix.homeserver') . '/_matrix/client/r0/login');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

                $data = curl_exec($ch);
                $data = json_decode($data);

                $data->expires = time() + 60;

                // var_dump($data);
                // Store in session
                $this->_user->matrix = $data;
                $_SESSION['user'] = $this->_user;

                return $data;
            } else if ($type == "login") {
                $expires = round(microtime(true) * 1000) + 60000;
                return Macaroon::create('https://' . \dtw\DtW::$config->get('matrix.homeserver'), "login", \dtw\DtW::$config->get('matrix.macaroon'))
                    ->withFirstPartyCaveat('gen = 1')
                    ->withFirstPartyCaveat('type = login')
                    ->withFirstPartyCaveat('guest = true')
                    ->withFirstPartyCaveat('user_id = @' . $this->_user->username . ':' . \dtw\DtW::$config->get('matrix.homeserver'))
                    ->withFirstPartyCaveat('time < ' . $expires)
                    ->withFirstPartyCaveat('nonce = ' . \dtw\utils\Utils::generateToken(15))
                    ->serialize();
            }
        }
    }
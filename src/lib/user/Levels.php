<?php
    namespace dtw\user;

    class Levels {
        private $_levels;

        public function __construct($id) {
            $this->id = $id;
            if (isset($_SESSION['userLevels'])) {
                $this->_levels = $_SESSION['userLevels'];
            } else {
                $this->regenerate();
            }
        }

        public function regenerate() {
            $this->_levels = array();

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `level_id`, `started`, `finished`
                FROM user_levels
                WHERE `user_id` = :id
            ');
            $stmt->execute(array(':id' => $this->id));

            $data = $stmt->fetchAll();

            foreach($data AS &$level) {
                if (isset($level->finished) && $level->finished > 0) {
                    $level->hasCompleted = true;
                } else {
                    $level->hasCompleted = false;
                }

                $this->_levels[$level->level_id] = $level;
            }

            $_SESSION['userLevels'] = $this->_levels;
        }

        public function getCompleted() {
            return array_filter($this->_levels, function($level) {
                return $level->hasCompleted;
            });
        }

        public function __get($name) {
            if (array_key_exists($name, $this->_levels)) {
                return $this->_levels[$name];
            }

            return null;
        }

        public function __isset($name) {
            if (array_key_exists($name, $this->_levels)) {
                return true;
            }

            return false;
        }
    }
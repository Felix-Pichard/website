<?php
    namespace dtw;

    class Statistics {
        /**
        * Get user list sorted by reputation
        *
        * @return array user IDs
        */
        public static function getLeaderboard($sort = 'reputation') {
            $key = 'stats:leaderboard:' . $sort;
            $leaderboard = \dtw\DtW::$redis->get($key);

            if ($leaderboard) {
                $leaderboard = json_decode($leaderboard);
            } else {
                $sort2 = $sort == 'points' ? 'reputation' : 'points';

                $stmt = \dtw\DtW::$db->prepare("
                    SELECT `users`.`user_id`
                    FROM `users`
                    LEFT OUTER JOIN `user_settings`
                    ON `user_settings`.`user_id` = `users`.`user_id` AND `user_settings`.`key` = 'privacy.leaderboard' AND `user_settings`.`value` = 1
                    WHERE `user_settings`.`user_id` IS NULL
                    ORDER BY `{$sort}` DESC, `{$sort2}` DESC, `username` ASC
                    LIMIT 25
                ");
                $stmt->execute();
                if ($stmt->rowCount()) {
                    $leaderboard = $stmt->fetchAll(\PDO::FETCH_COLUMN);

                    \dtw\DtW::$redis->set($key, json_encode($leaderboard));
                    \dtw\DtW::$redis->expire($key, 900);
                }
            }

            return $leaderboard;
        }

        public static function getChatStats() {
            $path = __DIR__ . '/../../matrix-stats/stats.json';
            if (!file_exists($path)) {
                return;
            }

            $stats = file_get_contents($path);

            $stats = json_decode($stats, true);

            $stats['today'] = $stats['days'][end(array_keys($stats['days']))];
            $stats['week'] = array_sum($stats['days']);

            // Sort users by lines spoken
            uasort($stats['users'], function ($item1, $item2) {
                return $item2['lines'] <=> $item1['lines'];
            });

            // Generate percentages for user times
            foreach($stats['users'] AS &$user) {
                $user['when'] = array(
                    'night' => 0,
                    'morning' => 0,
                    'afternoon' => 0,
                    'evening' => 0,
                );

                if ($user['hours']) {
                    $user['recordedLines'] = 0;
                    foreach($user['hours'] AS $hour => $lines) {
                        if ($hour <= 5) {
                            $user['when']['night'] += $lines;
                        } else if ($hour <= 11) {
                            $user['when']['morning'] += $lines;
                        } else if ($hour <= 17) {
                            $user['when']['afternoon'] += $lines;
                        } else {
                            $user['when']['evening'] += $lines;
                        }
                        $user['recordedLines'] += $lines;
                    }
                }
            }

            return $stats;
        }

    }
<?php
    namespace dtw\discussion;

    class Post extends \dtw\utils\DataObject {
        protected $type = 'discussion:post';
        protected $table = 'forum_thread_posts';

        protected function getByID($id) {
            // Check if thread exists in Redis
            $post = \dtw\DtW::$redis->get($this->type . ':' . $id);

            if ($post) {
                $this->data = json_decode($post);
                \dtw\DtW::$log->info('post.view', array('title' => $this->data->id, 'source' => 'redis'));
            } else {
                // Rebuild post from DB
                $stmt = \dtw\DtW::$db->prepare("
                    SELECT `post_id` AS `id`, `message`, `thread_id`, `posted`, IF(`updated` = 0, null, `updated`) AS `updated`, `deleted`, `author`, `reply_to`
                    FROM {$this->table}
                    WHERE `post_id` = :postID"); 
                $stmt->execute(array(':postID' => $id)); 
                if (!$stmt->rowCount()) {
                    throw new \Exception('Post not found ' . $id);
                }
                $this->data = $stmt->fetch();

                $this->generate();

                $this->updateCache();
                \dtw\DtW::$log->info('post.view', array('title' => $this->data->id, 'source' => 'DB'));
            }

            $this->id = $this->data->id;

            // Can current user edit this post?
            $DtW = \dtw\DtW::getInstance();
            $this->isAuthor = ($this->data->author == $DtW->user->id);
            if ($this->isAuthor || $DtW->user->hasPrivilege('discussions.post_edit')) {
                $this->isEditable = true;
            }

            if ($this->data->deleted == 1) {
                $this->isEditable = false;
            }
        }

        public function create($thread, $data) {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth()) {
                throw new \Exception('You must be logged in');
            }

            if (!isset($data['message']) || empty($data['message'])) {
                throw new \Exception('All fields are required');
            }

            if (isset($data['replyTo'])) {
                // TODO: secure if required
                $replyTo = $data['replyTo'];
            }

            $stmt = \dtw\DtW::$db->prepare('
                INSERT INTO forum_thread_posts (`message`, `thread_id`, `author`, `reply_to`)
                VALUES (:message, :thread, :author, :replyTo);
            ');
            $stmt->execute(array(
                ':message' => $data['message'],
                ':thread' => $thread,
                ':author' => $DtW->user->id,
                ':replyTo' => $replyTo
            ));

            $this->id = \dtw\DtW::$db->lastInsertId();

            return $this->id;
        }

        public function edit($message) {
            if (!$this->isEditable) {
                throw new \Exception('Invalid request');
            }

            if (!$message || strlen($message) <= 3) {
                throw new \Exception('Post body is missing or is too short');    
            }

            $this->data->message = $message;
            $this->data->updated = time();

            $stmt = \dtw\DtW::$db->prepare('
                UPDATE forum_thread_posts SET `message` = :message, `updated` = NOW() WHERE `post_id` = :post_id;
            ');
            $stmt->execute(array(
                ':message' => $this->data->message,
                ':post_id' => $this->id
            ));

            // Regenerae caches
            $this->generate();
            $this->updateCache();

            // Update thread search
            $thread = new \dtw\discussion\Thread($this->data->thread_id);
            $thread->buildSearch();
        }

        public function delete($force = false) {
            if (!$force && !$this->isEditable) {
                throw new \Exception('Invalid request');
            }

            $stmt = \dtw\DtW::$db->prepare('
                UPDATE forum_thread_posts SET `deleted` = 1 WHERE `post_id` = :post_id;
            ');
            $stmt->execute(array(
                ':post_id' => $this->id
            ));
            $this->clearCache();

            // Update thread search
            $thread = new \dtw\discussion\Thread($this->data->thread_id);
            $thread->buildSearch();
        }

        public function checkPost() {
            // Have they been posting alot
            $stmt = \dtw\DtW::$db->prepare("SELECT COUNT(*) AS `posts` FROM {$this->table} WHERE `author` = :user AND `posted` >= NOW() - INTERVAL 2 MINUTE");
            $stmt->execute(array(':user' => $this->data->author));
            $results = $stmt->fetch();
            if ($results->posts > 5) {
                $moderation = 'spam';
                $comment = 'This user has posted multiple posts in quick succession';
            }

            // Is this thread old?
            $stmt = \dtw\DtW::$db->prepare("SELECT (`posted` < NOW() - INTERVAL 2 MONTH) AS `old` FROM `forum_thread_posts` WHERE `thread_id` = :thread AND `post_id` != :post_id ORDER BY `posted` DESC LIMIT 1");
            $stmt->execute(array(
                ':thread' => $this->data->thread_id,
                ':post_id' => $this->id
            ));
            $results = $stmt->fetch();
            if ($results->old == 1) {
                $moderation = 'late';
                $comment = 'This thread has been quiet for a long time before this post was made';
            }

            // Is it a new user
            $stmt = \dtw\DtW::$db->prepare("SELECT `created` FROM `users` WHERE `user_id` = :user AND `created` >= NOW() - INTERVAL 2 HOUR");
            $stmt->execute(array(':user' => $this->data->author));
            if ($stmt->rowCount()) {
                $moderation = 'newuser';
                $comment = 'This user has posted quickly after joining the site';
            }


            if ($moderation) {
                // Report post
                $stmt = \dtw\DtW::$db->prepare("
                    INSERT INTO `forum_flags` (`post_id`, `user_id`, `type`, `comment`)
                    VALUES (:post_id, null, :type, :comment);
                ");
                $stmt->execute(array(
                    ':post_id' => $this->id,
                    ':type' => $moderation,
                    ':comment' => $comment
                ));
            }
        }

        public function karma($up = true) {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth()) {
                throw new \Exception('You must be logged in');
            }

            // Can user vote
            if ($this->isAuthor) {
                throw new \Exception('You can\'t award karma to yourself');
            }

            if (!$up && $DtW->user->profile->reputation < 50) {
                throw new \Exception('You don\'t have enough reputation to down vote');
            }

            // Insert or update
            $stmt = \dtw\DtW::$db->prepare('
                INSERT INTO forum_karma (`post_id`, `user_id`, `vote`)
                VALUES (:post_id, :user_id, :vote)
                ON DUPLICATE KEY UPDATE `vote` = :vote
            ');
            $stmt->execute(array(
                ':post_id' => $this->id,
                ':user_id' => $DtW->user->id,
                ':vote' => $up ? 'up' : 'down',
            ));

            // Get new total
            $this->updateKarma();
            $this->updateCache();

            // Check users medals
            \dtw\Medals::checkUsersMedals('karma', $this->data->author);

            if ($up) {
                \dtw\Medals::awardMedal('Rewarder');
            }

            if ($stmt->rowCount() === 0) {
                throw new \Exception('Karma had already been awarded for this post');
            }
        }

        private function updateKarma() {
            $this->data->karma = 0;
            $stmt = \dtw\DtW::$db->prepare("
                SELECT SUM(case when `vote` = 'up' then 1 else -1 end) as `karma` FROM `forum_karma` WHERE post_id = :postID GROUP BY post_id"); 
            $stmt->execute(array(':postID' => $this->id)); 
            $this->data->karma = $stmt->fetchColumn();
        }

        private function generate() {
            // Convert markdown
            $purifier = new \dtw\utils\Purifier();

            $message = $this->data->message;
            $this->data->message = new \stdClass();
            $this->data->message->plain = $message;
            $this->data->message->safe = htmlspecialchars($message);

            // Render message
            $html = \dtw\utils\Markdown::parse($message, true);

            $this->data->message->html = $html;

            // Count karma
            $this->updateKarma();
        }

        private function clearCache() {
            \dtw\DtW::$redis->del('discussions');
            \dtw\DtW::$redis->del('discussions:' . $this->data->thread_id);
            \dtw\DtW::$redis->del($this->type . ':' . $this->id);
        }
    }
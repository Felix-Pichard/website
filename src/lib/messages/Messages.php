<?php
    namespace dtw\messages;

    class Messages {

        static public function getAll($page = 1) {
            $key = 'pm:user:conversations:' . \dtw\DtW::getInstance()->user->id;

            $convoPerPage = 20;

            $conversations = \dtw\DtW::$redis->get($key);
            if ($conversations && $page === 1) {
                $all = json_decode($conversations);
            } else {
                $limitStart = ($page * $convoPerPage) - $convoPerPage;

                $stmt = \dtw\DtW::$db->prepare("
                    SELECT pm_users.pm_id AS `id`, pm_users.seen, pm_users.deleted
                    FROM pm_users
                    LEFT JOIN pm_messages
                        ON message_id = (SELECT message_id FROM pm_messages WHERE pm_messages.pm_id = pm_users.pm_id ORDER BY time DESC LIMIT 1)
                    WHERE pm_users.user_id = :userID
                    ORDER BY `pm_messages`.time DESC
                    LIMIT $limitStart, $convoPerPage
                "); 
                $stmt->execute(array(':userID' => \dtw\DtW::getInstance()->user->id));
                $all = $stmt->fetchAll();

                if ($page === 1) {
                    \dtw\DtW::$redis->set($key, json_encode($all));
                    \dtw\DtW::$redis->expire($key, 900);
                }
            }

            $conversations = array();
            foreach($all AS $convo) {
                try {
                    $conversation = new Conversation($convo->id);

                    $conversation->seen = $convo->seen;
                    $conversation->deleted = $convo->deleted;

                    array_push($conversations, $conversation);
                } catch (\Exception $e) {
                    //
                }
            }

            return $conversations;
        }

        static public function getUnread() {
            $key = 'pm:user:unread:' . \dtw\DtW::getInstance()->user->id;

            $count = \dtw\DtW::$redis->get($key);
            if ($count === null) {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT count(*)
                    FROM pm_users
                    LEFT JOIN pm_messages
                        ON message_id = (SELECT message_id FROM pm_messages WHERE pm_messages.pm_id = pm_users.pm_id ORDER BY time DESC LIMIT 1)
                    WHERE pm_users.user_id = :userID
                        AND pm_messages.time > pm_users.seen
                '); 
                $stmt->execute(array(':userID' => \dtw\DtW::getInstance()->user->id));
                $count = $stmt->fetch(\PDO::FETCH_COLUMN);

                \dtw\DtW::$redis->set($key, $count);
                \dtw\DtW::$redis->expire($key, 900);
            }

            return $count;
        }

        static public function getConversation($ID) {
            return new \dtw\messages\Conversation($ID);
        }

        static public function getConversationByRecipient($to) {
            $DtW = \dtw\DtW::getInstance();

            $sql = <<<SQL
                SELECT pm_users.pm_id, count(*) AS matches, users2.recipients
                FROM pm_users
                INNER JOIN (SELECT pm_id, count(*) AS recipients FROM pm_users GROUP BY pm_id) users2
                ON pm_users.pm_id = users2.pm_id
                WHERE user_id = :user1 or user_id = :user2
                GROUP BY pm_users.pm_id
                HAVING matches = recipients AND matches = 2
                ORDER BY pm_users.pm_id DESC
                LIMIT 1
SQL;

                $stmt = \dtw\DtW::$db->prepare($sql); 
                $stmt->execute(array(
                    ':user1' => $DtW->user->id,
                    ':user2' => $to
                ));

                return $stmt->fetch(\PDO::FETCH_COLUMN);
        }

        static public function newConversation($recipients, $message) {
            $DtW = \dtw\DtW::getInstance();

            if (!$message || strlen(trim($message)) < 5) {
                throw new \Exception("Message is not long enough");
            }

            try {
                \dtw\DtW::$db->beginTransaction();

                // Get a new PM ID
                $stmt = \dtw\DtW::$db->prepare('INSERT INTO `pm` VALUES (NULL)'); 
                $stmt->execute();

                $convoID = \dtw\DtW::$db->lastInsertId();

                // Add message
                $stmt = \dtw\DtW::$db->prepare('INSERT INTO `pm_messages` (`pm_id`, `user_id`, `message`) VALUES (:id, :userID, :message)'); 
                $stmt->execute(array(
                    ':id' => $convoID,
                    ':userID' => $DtW->user->id,
                    ':message' => $message
                ));

                // Add current user
                $stmt = \dtw\DtW::$db->prepare('INSERT INTO `pm_users` (`pm_id`, `user_id`, `seen`) VALUES (:id, :userID, NOW())'); 
                $stmt->execute(array(
                    ':id' => $convoID,
                    ':userID' => $DtW->user->id
                ));
                $sent = array($DtW->user->id);

                // Add recipients
                $notificationData = (object) array(
                    'conversation' => $convoID
                );

                $recipients = preg_split('/[,;]/', $recipients);
                foreach($recipients AS $recipient) {
                    $recipient = trim($recipient);

                    // Lookup user by username
                    try {
                        $profile = \dtw\Users::getUser($recipient);
                    } catch (\Exception $e) {
                        continue;
                    }

                    $userID = $profile->id;

                    if (in_array($userID, $sent)) {
                        continue;
                    }

                    // Can this user send a message to this user?
                    $privacy = \dtw\user\Settings::getUserSetting($userID, 'privacy.pm');
                    if ($privacy) {
                        // Are they folowing me?
                        if (!in_array($DtW->user->id, $profile->following)) {
                            throw new \Exception('One or more recipients have disabled PMs');
                        }
                    }

                    if ($profile->isBlocked()) {
                        throw new \Exception('You can\'t messages blocked users');
                    } elseif ($profile->hasBlocked()) {
                        throw new \Exception('You are unable to reply to this conversation');
                    }

                    $stmt = \dtw\DtW::$db->prepare('INSERT INTO `pm_users` (`pm_id`, `user_id`) VALUES (:id, :userID)'); 
                    $stmt->execute(array(
                        ':id' => $convoID,
                        ':userID' => $userID
                    ));

                    array_push($sent, $userID);
                }

                if (count($sent) > 15) {
                    throw new \Exception('Only 15 recipient allowed per thread');
                }

                \dtw\DtW::$db->commit();
            } catch (\Exception $e) {
                \dtw\DtW::$db->rollback();

                throw $e;
            }

            $key = 'pm:user:conversations:' . \dtw\DtW::getInstance()->user->id;
            \dtw\DtW::$redis->del($key);

            // Notify all recipents
            foreach($sent AS $userID) {
                if ($userID == $DtW->user->id) continue;
                \dtw\Notifications::send($userID, 'message.new', $notificationData);

                // Clear their conversation cache
                $key = 'pm:user:conversations:' . $userID;
                \dtw\DtW::$redis->del($key);
                $key = 'pm:user:unread:' . $userID;
                \dtw\DtW::$redis->del($key);
            }

            return $convoID;
        }
    }
<?php
    namespace dtw;

    class Articles {
        private static $topics;
        private static $picks;

        public static function registerHooks() {
            \dtw\DtW::$api->registerHook('articles', 'view', function($params) {
                if (!isset($_POST['id'])) {
                    throw new \Exception('Invalid ID');
                }

                self::getArticle($_POST['id'])->countView();
                return true;
            });
        }

        public static function getArticle($slug) {
            return new articles\Article($slug);
        }

        public static function getTopic($slug) {
            return new articles\Topic($slug);
        }

        public static function getTopics() {
            if (isset(self::$topics)) {
                return self::$topics;
            }

            $topics = \dtw\DtW::$redis->get('articles:topics');

            if ($topics) {
                $topics = json_decode($topics);
            } else {
                $purifier = new utils\Purifier();

                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `topic_id`
                    FROM `article_topics`
                    ORDER BY `order` ASC, `title` ASC
                ');
                $stmt->execute();
                if ($stmt->rowCount()) {
                    $topics = $stmt->fetchAll();

                    foreach($topics AS &$topic) {
                        $t = new articles\Topic($topic->topic_id);
                        $topic = $t->getData();
                    }

                    \dtw\DtW::$redis->set('articles:topics', json_encode($topics));
                } else {
                    throw new \Exception('No topics found');
                }
            }

            // Store for later use
            self::$topics = $topics;

            return $topics;
        }

        public static function getArticles($args = array()) {
            $params = array();
            $sql = 'SELECT `article_id` AS `id`, `title` FROM articles WHERE `status` = "published"';

            if ($args['q']) {
                $sql .= ' AND `title` LIKE :q';
                $params[':q'] = "{$args['q']}";
            }

            $orderByAllows = array('published', 'title', 'views');
            if ($args['orderBy'] && in_array(strtolower($args['orderBy']), $orderByAllows)) {
                $order = strtolower($args['order']) == 'asc' ? 'ASC' : 'DESC';
                $orderBy = $args['orderBy'];
                $sql .= " ORDER BY `{$orderBy}` {$order}";
            }

            if ($args['limit']) {
                $limit = intval($args['limit']);
                $sql .= " LIMIT {$limit}";
            } else {
                $sql .= " LIMIT 1000";
            }

            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute($params); 
            if ($stmt->rowCount()) {
                $articles = $stmt->fetchAll();

                return $articles;
            }
        }

        public static function getPicks() {
            if (isset(self::$picks)) {
                return self::$picks;
            }

            $picks = \dtw\DtW::$redis->get('articles:picks');

            if ($picks) {
                $picks = json_decode($picks);
            } else {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `article_id`
                    FROM `articles`
                    WHERE `status` = "published"
                    ORDER BY `published` DESC, `title` ASC
                    LIMIT 15;
                '); 
                $stmt->execute();
                if ($stmt->rowCount()) {
                    $picks = $stmt->fetchAll(\PDO::FETCH_COLUMN);

                    \dtw\DtW::$redis->set('articles:picks', json_encode($picks));
                    \dtw\DtW::$redis->expire('articles:picks', 300);
                }
            }

            // Store for later use
            self::$picks = $picks;

            return $picks;
        }

        public static function getStats() {
            $key = 'articles:stats';
            $stats = \dtw\DtW::$redis->get($key);

            if ($stats) {
                $stats = json_decode($stats);
            } else {
                $stats = array();

                $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `articles` FROM `articles`'); 
                $stmt->execute();
                $row = $stmt->fetch();
                $stats['count'] = $row->articles;

                $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `articles` FROM `articles` WHERE `status` = "published"'); 
                $stmt->execute();
                $row = $stmt->fetch();
                $stats['published'] = $row->articles;

                \dtw\DtW::$redis->set($key, json_encode($stats));
                \dtw\DtW::$redis->expire($key, 900);
            }

            return $stats;
        }

        public static function getArticleEditorForm($article = null) {
            if ($article) {
                $form = new \dtw\utils\Form($article->title, "Update", array('block' => true));
            } else {
                $form = new \dtw\utils\Form("New article", "Submit", array('block' => true));
            }

            $form->addField('Title', "text", array('value' => isset($article->title) ? $article->title : '' ));
            $form->addField('Content', "markdown", array('value' => isset($article->content->plain) ? $article->content->plain : '' ));

            return $form;
        }

        public static function create($data) {
            $article = new articles\Article();
            $article->create($data);

            return $article;
        }

        public static function saveViewCounts() {
            if (!defined('CRON')) {
                throw new \Exception('Method not run from CRON');
            }

            // Get all thread IDs in redis
            $key = 'articles:updated';

            do {
                $updated = \dtw\DtW::$redis->sPop($key);
                if ($updated) {
                    $article = self::getArticle($updated);
                    $article->storeViews();
                }
            } while($updated);
        }

    }
index index.html index.php;
server_tokens off;
client_max_body_size 25M;

# Force the latest IE version
add_header "X-UA-Compatible" "IE=Edge";

# gzip
gzip  on;
gzip_http_version 1.1;
gzip_vary on;
gzip_comp_level 6;
gzip_proxied any;
gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript image/svg+xml application/javascript;
gzip_buffers 16 8k;
# Disable gzip for certain browsers.
gzip_disable “MSIE [1-6].(?!.*SV1)”;

# NGINX file caching
open_file_cache          max=1000 inactive=20s;
open_file_cache_valid    30s;
open_file_cache_min_uses 2;
open_file_cache_errors   off;

add_header X-Frame-Options SAMEORIGIN always;
add_header X-Content-Type-Options nosniff always;
add_header Referrer-Policy no-referrer always;
add_header X-XSS-Protection "1";

add_header "Cache-Control" "no-transform";
charset utf-8;

error_page 404 /404.html;

location ~* /\.(?!well-known\/) {
    deny all;
}

location ~* (?:\.(?:bak|conf|dist|fla|in[ci]|log|psd|sh|sql|sw[op])|~)$ {
    deny all;
}

location ~* \.(?:manifest|appcache|html?|xml|json)$ {
    expires -1;
}

# Feed
location ~* \.(?:rss|atom)$ {
    expires 1h;
    add_header "Cache-Control" "public";
}
FROM php:7.3-fpm

ENV php_conf /usr/local/etc/php-fpm.conf
ENV fpm_conf /usr/local/etc/php-fpm.d/www.conf
ENV php_vars /usr/local/etc/php/conf.d/docker-vars.ini

RUN apt-get update -yqq

RUN apt-get install gnupg -yqq

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -

RUN apt-get install nano nginx nodejs npm redis mariadb-server-10.3 discount libmagickwand-dev imagemagick libzip-dev libcurl4-gnutls-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev wget graphviz graphviz-dev -yqq

RUN yes "" | pecl install imagick

RUN npm install -g npm@latest

# ElasticSearch
RUN wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add - \
    && echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" >> /etc/apt/sources.list \
    && apt-get update -yqq \
    && apt-get install elasticsearch -yqq


RUN apt-get install -y zip libzip-dev \
  && docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip

RUN docker-php-ext-install mbstring pdo_mysql curl json intl gd xml bz2 opcache

RUN pecl install xdebug

RUN docker-php-ext-enable xdebug

RUN curl -sS https://getcomposer.org/installer | php

RUN mv composer.phar /usr/local/bin/composer

# RUN wget http://phpdoc.org/phpDocumentor.phar
# RUN mv phpDocumentor.phar /usr/local/bin/phpDocumentor
# RUN chmod +x /usr/local/bin/phpDocumentor

RUN echo 'error_reporting = E_COMPILE_ERROR|E_RECOVERABLE_ERROR|E_ERROR|E_CORE_ERROR' > /usr/local/etc/php/conf.d/50-errors.ini

# Copy our nginx config
ADD config/base.conf /etc/nginx/base.conf

# nginx site conf
RUN mkdir -p /etc/nginx/sites-available/ && \
mkdir -p /etc/nginx/sites-enabled/ && \
rm -Rf /var/www/* && \
rm /etc/nginx/sites-available/default
ADD config/dtw.conf /etc/nginx/sites-available/default

# change php listen
# RUN mkdir -p /run/php-fpm/ && chmod 777 /run/php-fpm/
# RUN sed -i 's/listen = 127\.0\.0\.1:9000/listen = \/run\/php-fpm\/php-fpm\.sock/g' /usr/local/etc/php-fpm.d/www.conf

# setup mysql
RUN sed -i "s/127\.0\.0\.1/0.0.0.0/g" /etc/mysql/mariadb.conf.d/50-server.cnf

# tweak php-fpm config
RUN echo "cgi.fix_pathinfo=0" > ${php_vars} &&\
    echo "upload_max_filesize = 100M"  >> ${php_vars} &&\
    echo "post_max_size = 100M"  >> ${php_vars} &&\
    echo "variables_order = \"EGPCS\""  >> ${php_vars} && \
    echo "memory_limit = 128M"  >> ${php_vars} && \
    echo "extension=imagick.so"  >> ${php_vars}

EXPOSE 80

ADD scripts/start.sh /start.sh
RUN chmod +x /start.sh
CMD ["/start.sh"]

WORKDIR /var/www